#!/usr/bin/env bash


# Handles local computation through command line, both data preprocessing and screening.
# Either computes weights or test them


atr=$1      # active_train.sdf
itr=$2      # inactive_train.sdf
ava=$3      # active_validation.sdf
iva=$4      # inactive_validation.sdf
ate=$5      # active_test.sdf
ite=$6      # inactive_test.sdf
w=$7        # file with weights
o=$8        # output file
tmp=$9      # tmp directory for computation
f=${10}     # fragment type [ecfp.1, ecfp.2, tt.2, tt.3, tt.4]
p=${11}     # fragment noise treshold percentage [0-100]
d=${12}     # descriptor type [RDKit, PaDEL]
sb=${13}    # scaling or binning [scaling, binning]
corr=${14}  # correlation treshold [0-1]
m=${15}     # similarity function (method) [eucldiean, simple, manhattan, Nfrag versions as euclidean[0-100], manhattan[0-100], simple[0-100]]
a=${16}     # annealing type [none, rand_start, const_start]
T=${17}     # starting simulated annealing temperature [>1]
dist=${18}  # dostance of weights percentage [0-100]
parts=${19} # number of dimensions to change in a iteration [>0]
c=${20}     # cooling [>0]
type=${21}  # model creation (finding weights) or testing weights [model, test]
dir=${22}   # directory of WeFrag (in order to find subdirectory with code)
seed=${23}  # seed for random number generation

   mkdir -p $tmp
   decoys_sdf=$tmp/"Decoys.sdf"
   ligands_sdf=$tmp/"Ligands.sdf"
   decoys_sdf_tmp=$tmp/"Decoys_tmp.sdf"
   ligands_sdf_tmp=$tmp/"Ligands_tmp.sdf"
   target_short=$target


    # aggregate decoys into 1 file
   python3 $dir/Code_config/merge_sdf.py -te $ite -tr $itr -va $iva -o $decoys_sdf_tmp
   if [ $? -ne 0 ]; then
       echo "Decoys_tmp.sdf generation error - DO YOU HAVE DATA IN THE RIGHT DIRECTORY??"
       exit
   fi

    # remove duplicates
   #Remove duplicate molecules to reduce the size of .sdf
   python3 $dir/Code_config/remove_duplicates.py -i $decoys_sdf_tmp -o $decoys_sdf
   rm $decoys_sdf_tmp


    # aggregate actives into 1 file
   python3 $dir/Code_config/merge_sdf.py -te $ate -tr $atr -va $ava -o $ligands_sdf_tmp
   if [ $? -ne 0 ]; then
       echo "Ligands_tmp.sdf generation error - DO YOU HAVE DATA IN THE RIGHT DIRECTORY??"
       exit
   fi

    # remove duplicates
   #Remove duplicate molecules to reduce the size of .sdf
   python3 $dir/Code_config/remove_duplicates.py -i $ligands_sdf_tmp -o $ligands_sdf
   rm $ligands_sdf_tmp


    # generate info about split
   python3 $dir/Code_config/split_info.py -ted $ite -trd $itr -vad $iva -tel $ate -trl $atr -val $ava -o $tmp/"split"
   if [ $? -ne 0 ]; then
       echo "Split info generation error - DO YOU HAVE DATA IN THE RIGHT DIRECTORY??"
       exit
   fi


    # generate fragments for decoys
   python3 $dir/Code_config/extract_fragments.py -i $decoys_sdf -f $f -o $tmp/"decoys_fragments"
   if [ $? -ne 0 ]; then
       echo "Decoys fragments generation error - DO YOU HAVE RDKIT ENABLED??"
       exit
   fi


    # generate fragments for ligands
   python3 $dir/Code_config/extract_fragments.py -i $ligands_sdf -f $f -o $tmp/"ligands_fragments"
   if [ $? -ne 0 ]; then
       echo "Ligands fragments generation error - DO YOU HAVE RDKIT ENABLED??"
       exit
   fi

    # generate PaDEL descriptors for fragments
    if [ "$d" = "PaDEL" ]; then

            # Generates teomporary data for PaDEL descriptor generator
            python3 $dir/Code_config/padel_descriptors.py -i $tmp/"ligands_fragments" -o $tmp/"ligands_desc_temp" -f > /dev/null 2>&1
            if [ $? -ne 0 ]; then
                echo "Ligands temp generation error - DO YOU HAVE PaDEL in Code_config??"
                exit
            fi

            # Generates raw PaDEL descriptors for fragments
            java -jar $dir/Code_config/PaDel/PaDEL-Descriptor.jar -maxruntime 5000 -2d -dir $tmp/"ligands_desc_temp" -file $tmp/"ligands_desc_raw" > /dev/null 2>&1
            if [ $? -ne 0 ]; then
                echo "Ligands raw desc generation error"
                exit
            fi


            python3 $dir/Code_config/padel_descriptors.py -i $tmp/"decoys_fragments" -o $tmp/"decoys_desc_temp" -f > /dev/null 2>&1
            if [ $? -ne 0 ]; then
                echo "Decoys temp generation error - DO YOU HAVE PaDEL in Code_config??"
                exit
            fi

            java -jar $dir/Code_config/PaDel/PaDEL-Descriptor.jar -maxruntime 5000 -2d -dir $tmp/"decoys_desc_temp" -file $tmp/"decoys_desc_raw" > /dev/null 2>&1
            if [ $? -ne 0 ]; then
                echo "Decoys raw desc generation error"
                exit
            fi
    fi

    # generate RDKit for decoys for fragments
    if [ "$d" = "RDKit" ]; then

            # Generates raw RDKit descriptors
            python3 $dir/Code_config/rdkit_descriptors.py -i $tmp/"ligands_fragments" -o $tmp/"ligands_desc_raw" --fragments
            if [ $? -ne 0 ]; then
                echo "Ligands raw desc generation error - DO YOU HAVE RDKIT ENABLED??"
                exit
            fi

            python3 $dir/Code_config/rdkit_descriptors.py -i $tmp/"decoys_fragments" -o $tmp/"decoys_desc_raw" --fragments
            if [ $? -ne 0 ]; then
                echo "Decoys raw desc generation error - DO YOU HAVE RDKIT ENABLED??"
                exit
            fi
    fi

    # Throws away fragments over treshold, reindexes fragments in molecules, creates better format for reading and removes and correlated descriptors
    if [ "$type" = "model" ]; then
        python3 $dir/Code_config/phase_preprocess.py -df $tmp/"decoys_fragments" -lf $tmp/"ligands_fragments" -s $tmp/"split" -t $p -c $corr -tmp False -p 1 -o $tmp/"phase_1" -dd $tmp/"decoys_desc_raw" -ld $tmp/"ligands_desc_raw" -m $sb
        if [ $? -ne 0 ]; then
            echo "Preprocessing error phase 1"
            exit
        fi

        result=`$dir/Code_config/screening.exe $tmp/"phase_1" "phase_2_dummy" $m $a $T $dist $parts $c $type "weights_dummy" $seed`; if [ $? -eq 0 ]; then echo $result; else echo \"Screening error\" fi; fi
    fi

    # Throws away fragments over treshold, reindexes fragments in molecules, creates better format for reading and removes and correlated descriptors
    if [ "$type" = "test" ]; then
        python3 $dir/Code_config/phase_preprocess.py -df $tmp/"decoys_fragments" -lf $tmp/"ligands_fragments" -s $tmp/"split" -t $p -p 2 -w $w -o $tmp/"phase_2" -dd $tmp/"decoys_desc_raw" -ld $tmp/"ligands_desc_raw" -m $sb
        if [ $? -ne 0 ]; then
            echo "Preprocessing error phase 2"
            exit
        fi

       result=`$dir/Code_config/screening.exe "phase_1_dummy" $tmp/"phase_2" $m $a $T $dist $parts $c $type $w $seed`; if [ $? -eq 0 ]; then echo $result; else echo \"Screening error\" fi; fi
    fi