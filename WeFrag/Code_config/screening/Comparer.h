

#ifndef SCREENING_EXE_COMPARER_H
#define SCREENING_EXE_COMPARER_H

#include <vector>
#include "val.h"
#include "Node.h"

/*
Parent class for the implementation of similarity functions
*/

class Comparer {
public:
    virtual void
    compare_sets(std::vector<int> *first_set, int first_num, std::vector<int> *second_set, int second_num, val **cache,
                 double **descriptors, int desc_len, std::vector<val> &best_compared, int method, bool activity,
                 double *weights) {};

    virtual void
    compare_sets(std::vector<int> *first_set, int first_num, std::vector<int> *second_set, int second_num, val **cache,
                 double **descriptors, int desc_len, std::vector<val> &best_compared, int method, bool activity,
                 double *weights, Node *linked_list, std::string phase) {};
};


#endif //SCREENING_EXE_COMPARER_H
