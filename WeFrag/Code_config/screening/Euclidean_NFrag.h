

#ifndef SCREENING_EXE_EUCLIDEAN_NFRAG_H
#define SCREENING_EXE_EUCLIDEAN_NFRAG_H


#include "Comparer.h"
#include <list>
#include "Node.h"

/*
Calculation of Euclidean similarity and Nfrag Euclidean similarity
*/

class Euclidean_NFrag : public Comparer {
public:
    virtual void
    compare_sets(std::vector<int> *first_set, int first_num, std::vector<int> *second_set, int second_num, val **cache,
                 double **descriptors, int desc_len, std::vector<val> &best_compared, int method, bool activity,
                 double *weights, Node *linked_list, std::string phase);

    float mols_distance(std::vector<int> &first_mol, std::vector<int> &second_mol, val **cache, double **descriptors,
                        int desc_len, float method, double *weights, Node *linked_list);
};


#endif //SCREENING_EXE_EUCLIDEAN_NFRAG_H
