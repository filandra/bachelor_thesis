

#ifndef SCREENING_EXE_NODE_H
#define SCREENING_EXE_NODE_H

#include <algorithm>

// Just a small struct which is used in linked list for sorting

struct Node {
    Node *next = NULL;
    double value = 1000000;
    int position;

    Node() {};

    Node(int new_pos) {
        position = new_pos;
    };
};

#endif //SCREENING_EXE_NODE_H
