

#ifndef SCREENING_OPTIMIZER_H
#define SCREENING_OPTIMIZER_H

/*
Parent class for the implementation of algorithms for the search of weights
*/

class Optimizer {
public:
    virtual void
    calc_weights(double *old_weights, double *weights, double &old_AUC, double new_AUC, double &T, double dist,
                 int parts, int desc_len, double &cooling, double orig_T, double starting_val) {};
};

#endif //SCREENING_OPTIMIZER_H
