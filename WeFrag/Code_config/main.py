#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Main logic of the WeFrag program. Handles communication with server, user interface and the retrieval of results

Usage:
	Either in a menu mode or through command line arguments
	Complete guide of usage can be found in user documentation.

-type - menu, model or test
-atr - active train
-itr - decoys train
-ate - active test
-ite - decoys test
-ava - active validation
-iva - decoys validation
-w - weights
-o - output
-tmp - tmp directory
-f - type of fragments [ecfp.1, ecfp.2, tt.2, tt.3, tt.4] - default ecfp.1
-p - fragment noise treshold percentage [0-100] - default 50
-d - descriptors used [RDKit, PaDEL]- default RDKit
-sb - scaling or binning of descriptor values [scaling, binning] - default scaling
-corr - correlation treshold [0-1] - default 0.5
-m - similarity function used [euclidean, manhattan, simple, euclidean[0-100], manhattan[0-100], simple[0-100]] - default eucldiean100
-a - annealing type [none, rand_start, const_start] - default const_start
-T - starting temperature - default 1000
-dist - distance of weights in annealing [0,100] - default 25
-parts - how many parts of weights vector to change in an iteration of annealing - default 1
-c - cooling rate - default 0.999
-seed - Seed used for random numbers generation (RANDOM seed by default)
"""

import threading
from queue import Queue
import time
import socket
from subprocess import Popen, PIPE, TimeoutExpired
import random
import os
import argparse
import math
import re
from collections import OrderedDict
import getpass
import sys


# Class holding the information about given task
class Task(object):
	def __init__(self):
		self.dataset = ""
		self.target = ""
		self.fragments = ""
		self.descriptor = ""
		self.percentage = ""
		self.method = ""
		self.script = ""
		self.split = ""
		self.scaling = ""
		self.corr = ""
		self.annealing_type = ""
		self.orig_T = 0
		self.distance = 0
		self.parts = 0
		self.cooling = 0;
		self.phase = "both";


# Class holding information about computers in cluster
class Computer(object):
	def __init__(self):
		self.name = ""
		self.adress = ""
		self.alive = False
		self.free = False
		self.started = False
		self.tasks = []
		self.connecting = False


# Class holding information and functions about given socket connection
class Client_Socket(object):
	def __init__(self, adress):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((adress, port))

	def send(self, message):
		self.socket.sendall(message.encode())

	def receive(self):  # Možná nějak lépe pořešit čeká na odpověd - tohle možná není nejlepší řešení
		response = self.socket.recv(4092).decode()
		return response

	def close(self):
		self.socket.close()


# Directory for the result of task computation
def task_directory(task):
	final_dir = ""
	final_dir = local_directory[
					0] + "/Results/" + task.dataset + "/" + task.target + "/" + task.fragments + "/" + task.descriptor + "/" + task.method + "/" + "corr_" + str(
		task.corr) + "/" + "noise_" + str(task.percentage) + "/" + task.annealing_type + "/" + "T_" + str(
		task.orig_T) + "/" + "cooling_" + str(task.cooling) + "/" + "dist_" + str(task.distance) + "/" + "dim_" + str(
		task.parts)
	return final_dir


# Interprets response of a subprocess
def interpret_response(response, task_comp):
	# If error occurs, the task will be reassigned for evaluation
	# Response from generate_data
	if task_comp[0].script == "generate_data.sh":
		with generates_lock:
			if response == "Done":
				computing_generates.remove(task_comp[0])
			else:
				# Error occured somewhere
				if response == "Timeout error":
					print("generate_data.sh timeout error: " + response + " " + get_params(task_comp[0]))
				else:
					print("generate_data.sh error: " + response + " " + get_params(task_comp[0]))
				computing_generates.remove(task_comp[0])
				waiting_generates.put(task_comp[0])

	# Response from screening
	if task_comp[0].script == "screening.exe":
		with screenings_lock:
			if response != "Screening error" or response != "Timeout error":
				computing_screenings.remove(task_comp[0])
				# Save the results
				with open(task_directory(task_comp[0]) + "/" + task_comp[0].split, "w+") as f:
					f.write(response.replace('\\n', '\n'))
			else:
				# Error occured somewhere
				if response == "Timeout error":
					print("screening.exe Timeout error: " + response + " " + get_params(task_comp[0]))
				else:
					print("screening.exe error: " + response + " " + get_params(task_comp[0]))
				computing_screenings.remove(task_comp[0])
				waiting_screenings.put(task_comp[0])

	# Process is no longer computed on this computer
	with cluster_lock:
		task_comp[1].tasks.remove(task_comp[0])
	return ""


def create_thread(function, params):
	thread = threading.Thread(target=function, args=params)
	thread.daemon = True
	thread.start()


def run_subprocess(args, limit):
	process = Popen(args, stdout=PIPE, stderr=PIPE, shell=True)
	try:
		stdout, stderr = process.communicate(timeout=limit)
		out = stdout.rstrip().decode()
		err = stderr.rstrip().decode()
	except TimeoutExpired:
		process.kill()
		out = "Timeout error"
		err = "Timeout error"
	return out, err


# Handles the input of information about targets to be computed
def gather_input_target(valid_inputs, dict):
	out = []
	s = input().split()
	print(dict)

	for item in s:
		if item == "home":
			return "home"
		if item == "all":
			out.clear()
			for dataset in dict:
				for target in dict[dataset]:
					out.append(target)
			return out
		if item in valid_inputs and item not in out:
			out.append(item)
		else:
			print("Invalid input!")
			out = gather_input(valid_inputs)

	if len(s) == 0:
		print("Invalid input!")
		out = gather_input(valid_inputs)
	return out  # "input in list"


# Handles the general input of information
def gather_input(valid_inputs):
	out = []
	s = input().split()

	for item in s:
		if item == "all":
			return valid_inputs
		if item == "home":
			return "home"
		if item in valid_inputs and item not in out:
			out.append(item)
		else:
			print("Invalid input!")
			out = gather_input(valid_inputs)

	if len(s) == 0:
		print("Invalid input!")
		out = gather_input(valid_inputs)
	return out  # "input in list"


# Runs a search of avaliable datasets and targets on client
def datasets_on_server():
	options = []
	dict = {}
	path = local_directory[0]
	data_path = path + "/Data"
	for dataset in os.listdir(data_path):
		if os.path.isdir(data_path + "/" + dataset):
			for target in os.listdir(data_path + "/" + dataset):
				if os.path.isdir(data_path + "/" + dataset + "/" + target):
					if dataset not in dict:
						dict[dataset] = []
						options.append(dataset)
					dict[dataset].append(target)
					options.append(target)
	return options, dict


# Reads the information about cluster computers from cluster_config.txt
def read_cluster_config():
	try:
		with open(local_directory[0] + "/Code_config/cluster_config.txt", 'r') as stream:
			# Skips first line
			lines = stream.readlines()
		for i in range(1, len(lines)):
			split = lines[i].split()
			if len(split) != 2:
				continue
			comp = Computer()
			comp.name = split[0]
			comp.adress = split[1]
			cluster_config.append(comp)
	except IOError:
		print("cluster_config.txt not found!")


# Tries to use the login information provided by user
def try_login():
	# Tries connecting to 3 random computers
	live_comps = []

	for comp in cluster_config:
		if comp.alive == True:
			live_comps.append(comp)

	if len(live_comps) == 0:
		print("No live computers in the cluster!")
		exit()

	for i in range(0, 3):
		comp = random.choice(live_comps)
		args = "sshpass -p \"" + log_pass[1] + "\" ssh -o StrictHostKeyChecking=no \"" + log_pass[
			0] + "@" + comp.name + "\"" + " \"echo passed\""
		limit = 3
		stdout, stderr = run_subprocess(args, limit)
		if stdout == "passed":
			return True

	print("Invalid login or password!")
	log_pass.clear()
	return False


# Handles situation when cluster computer dies
def comp_died(comp):
	comp.alive = False
	comp.free = False
	comp.started = False
	comp.connecting = False

	# Kills every running process on the computer
	if not log_pass == []:
		args = "sshpass -p " + log_pass[1] + " ssh -o StrictHostKeyChecking=no " + log_pass[
			0] + comp.name + " \"kill -9 -1\""
		limit = 3
		run_subprocess(args, limit)

	with generates_lock:
		with screenings_lock:
			for task in comp.tasks:
				if task.script == "generate_data.sh":
					computing_generates.remove(task)
					waiting_generates.put(task)
				if task.script == "screening.sh":
					computing_screenings.remove(task)
					waiting_screenings.put(task)
				comp.tasks.remove(task)


# Runs bash script to ping every computer in cluster
def ping():
	for comp in cluster_config:
		time.sleep(0.01)
		args = "bash " + local_directory[0] + "/Code_config/pinger.sh " + comp.name + " online"
		limit = 3
		stdout, stderr = run_subprocess(args, limit)
		if stdout == "alive":
			comp.alive = True
			print(comp.name + " alive")
		else:
			print(comp.name + " dead")
			comp_died(comp)


# Runs bash script to find the activity on each cluster computer
def cluster_activity():
	for comp in cluster_config:
		if comp.alive == True:
			args = "bash " + local_directory[0] + "/Code_config/pinger.sh " + comp.name + " activity " + log_pass[
				0] + " " + log_pass[1]
			limit = 5
			stdout, stderr = run_subprocess(args, limit)
			if stdout == "free":
				comp.free = True
				print(comp.name + " free")
			elif stdout == "taken":
				comp.free = False
				print(comp.name + " taken")
			else:
				comp_died(comp)
				print(comp.name + " died")
			# if error is returned


# Runs bash script to start server on each cluster computer
def cluster_start():
	for comp in cluster_config:
		if comp.alive == True and comp.free == True and comp.started == False:
			args = "bash " + local_directory[0] + "/Code_config/pinger.sh " + comp.name + " start " + log_pass[
				0] + " " + log_pass[1] + " " + cluster_directory[0] + "/Code_config"
			limit = 5
			stdout, stderr = run_subprocess(args, limit)
			if stdout == "started":
				comp.started = True
				print(comp.name + " started")
			else:
				comp_died(comp)
		elif comp.alive == True and comp.free == True and comp.started == True:
			# If someone is using the computer kills the computation
			args = "sshpass -p " + log_pass[1] + " ssh -o StrictHostKeyChecking=no " + log_pass[
				0] + comp.name + " \"ps aux | less\" | grep WeFrag_server.py"
			limit = 5
			stdout, stderr = run_subprocess(args, limit)
			if stdout == "":
				comp_died(comp)

# login when setting up the cluster
def setup_login():
	# Checks, if computer is online for every computer in cluster
	ping()
	# Acquires login and password, if not known already
	if not log_pass:
		ok = False
		while ok == False:
			log_pass.clear()
			print("Please enter login for your cluster.")
			login = input();
			print("Please enter password for your cluster.");
			password = getpass.getpass();
			log_pass.append(login)
			log_pass.append(password)
			ok = try_login()

# Refresh of the state of cluster
def cluster_refresh():
	# Checks, if computer is online for every computer in cluster
	ping()
	# Acquires login and password, if not known already
	if not log_pass:
		ok = False
		while ok == False:
			log_pass.clear()
			print("Please enter login for your cluster.")
			login = input();
			print("Please enter password for your cluster.");
			password = getpass.getpass();
			log_pass.append(login)
			log_pass.append(password)
			ok = try_login()
	# Chekcs, which computers are free to use
	cluster_activity()
	# Chekcs, which computers have their server scripts started
	cluster_start()


# Find a computer in cluster suitable to receive new task
def find_free_comp(task_type):
	# 8 tasks per computer achieves the best results in performance
	# Only 1 generation of data per computer (generate_data.sh)
	if task_type == "generate":
		for comp in cluster_config:
			if (
					not comp.tasks) and comp.alive == True and comp.free == True and comp.started == True and comp.connecting == False:
				return comp
		return "None"

	full = False
	if task_type == "screening":
		for comp in cluster_config:
			if len(comp.tasks) < max_tasks[
				0] and comp.alive == True and comp.free == True and comp.started == True and comp.connecting == False:
				for task in comp.tasks:
					if task.script == "generate_data.sh":
						full = True
				if full == False:
					return comp
		return "None"


# Get parameters need to run the command on server for a task
def get_params(task):
	dir = ""
	if location_info[0] == "local":
		dir = local_directory[0]
	else:
		dir = cluster_directory[0]

	if task.script == "generate_data.sh":
		split_dir = dir + "/Data/" + task.dataset + "/splits/" + task.target + "/"
		params = "bash " + dir + "/Code_config/generate_data.sh " + task.dataset + " " + task.target + " " + task.fragments + " " + task.percentage + " " + task.descriptor + " " + split_dir + " " + task.split + " " + task.scaling + " " + task.corr + " " + dir

	if task.script == "screening.exe":
		preproc_1 = dir + "/Preprocessed/" + task.dataset + "/" + task.target + "/" + task.fragments + "/" + task.descriptor + "/" + task.scaling + "/corr_" + task.corr + "/noise_" + task.percentage + "/" + task.split + "_phase_1"
		preproc_2 = dir + "/Preprocessed/" + task.dataset + "/" + task.target + "/" + task.fragments + "/" + task.descriptor + "/" + task.scaling + "/corr_" + task.corr + "/noise_" + task.percentage + "/" + task.split + "_phase_2"
		params = "result=`" + dir + "/Code_config/screening.exe " + preproc_1 + " " + preproc_2 + " " + task.method + " " + task.annealing_type + " " + str(
			task.orig_T) + " " + str(task.distance) + " " + str(task.parts) + " " + str(task.cooling) + " " + str(
			task.phase) + " " + " weigts_dummy" + " " + str(seed[0])
		params = params + "`; if [ $? -eq 0 ]; then echo $result; else echo \"Screening error\" fi; fi"
	return params


# Assigns task to a cluster computer
def command_cluster(task_comp):
	connected = True
	# Tries connection to remote server - catches any exception and tries again
	try:
		client = Client_Socket(task_comp[1].adress)
	except:
		try:
			time.sleep(0.1)
			client = Client_Socket(task_comp[1].adress)
		except:
			with cluster_lock:
				comp_died(task_comp[1])
			#print("Socket error handled for " + task_comp[1].name)
			connected = False

	if connected == True:

		# get parameters for a task to be run on the server
		script_param = get_params(task_comp[0])
		# Sends parameters to server
		client.send(script_param)
		task_comp[1].connecting = False

		# Checks if the server is not responding
		try:
			# Reads response from server - 16 reads to be sure we got the full buffer
			response = ""
			for item in range(0, 16):
				response += client.receive()
			response = response.rstrip()
		except:
			#print("Command error handled: " + script_param)
			if task_comp[0].script == "screening.exe":
				with screenings_lock:
					with cluster_lock:
						# Handles the faulty task
						computing_screenings.remove(task_comp[0])
						waiting_screenings.put(task_comp[0])
						task_comp[1].tasks.remove(task_comp[0])
			else:
				with generates_lock:
					with cluster_lock:
						# Handles the faulty task
						computing_generates.remove(task_comp[0])
						waiting_generates.put(task_comp[0])
						task_comp[1].tasks.remove(task_comp[0])
			client.close()
			return
		# Interprets response from server
		interpret_response(response, task_comp)
		client.close()


# Assignes task to local computer
def command_local(task_comp):
	# Gets the parameters of a task
	script_param = get_params(task_comp[0])
	try:
		# Runs the parameters as a subprocess
		response, err = run_subprocess(script_param, 5000)
	except:
		#print("Command error " + script_param)
		if task_comp[0].script == "screening.exe":
			with screenings_lock:
				with cluster_lock:
					# Handles faulty task
					computing_screenings.remove(task_comp[0])
					waiting_screenings.put(task_comp[0])
					task_comp[1].tasks.remove(task_comp[0])
		else:
			with generates_lock:
				with cluster_lock:
					# Handles faulty task
					computing_generates.remove(task_comp[0])
					waiting_generates.put(task_comp[0])
					task_comp[1].tasks.remove(task_comp[0])
		return

	interpret_response(response, task_comp)


# Checks if task would cause a runtime codition if it were started on a cluster
# task_collection is for example all runnig generations of data
def run_condition(task, task_collection):
	for item in task_collection:
		if task.dataset == item.dataset and task.target == item.target:
			return True
	return False


# Main cycle of the program - assignes tasks to local or cluster computer
def task_manager():
	counter = 0
	max_wait = 1000

	# Assignes generation of data first
	while True:
		with generates_lock:
			with screenings_lock:
				with cluster_lock:
					if not waiting_generates.empty():
						comp = find_free_comp("generate")
						if comp == "None":
							time.sleep(10)
							counter += 1
							if counter == max_wait:
								counter = 0
								if location_info[0] == "cluster":
									pass
								# cluster_refresh()
							continue
						else:
							counter = 0
							task = waiting_generates.get()
							# All splits cannot be computed at once - some cause runtime condition
							if run_condition(task, computing_generates):
								waiting_generates.put(task)
								continue
							computing_generates.append(task)
							comp.tasks.append(task)
							if location_info[0] == "cluster":
								comp.connecting = True
								create_thread(command_cluster, [(task, comp)])
							else:
								create_thread(command_local, [(task, comp)])
		time.sleep(0.1)

		# If all data generation is complete, assignes screenings
		with generates_lock:
			with screenings_lock:
				with cluster_lock:
					if waiting_generates.empty() and not computing_generates and not waiting_screenings.empty():
						comp = find_free_comp("screening")
						if comp == "None":
							time.sleep(10)
							counter += 1
							if counter == max_wait:
								counter = 0
								if location_info[0] == "cluster":
									pass
								# cluster_refresh()
							continue
						else:
							counter = 0;
							task = waiting_screenings.get()
							computing_screenings.append(task)
							comp.tasks.append(task)
							if location_info[0] == "cluster":
								comp.connecting = True
								create_thread(command_cluster, [(task, comp)])
							else:
								create_thread(command_local, [(task, comp)])
		time.sleep(0.1)

		if waiting_screenings.empty() and waiting_generates.empty():
			time.sleep(1)


# Checks if task is in a collection of tasks
def comparator_in(task, task_collection):
	for item in task_collection:
		if task.dataset == item.dataset and task.target == item.target and task.descriptor == item.descriptor and task.fragments == item.fragments and task.percentage == item.percentage and task.method == item.method and task.script == item.script and task.split == item.split and task.corr == item.corr and task.scaling == item.scaling and task.orig_T == item.orig_T and task.distance == item.distance and task.annealing_type == item.annealing_type and task.parts == item.parts and task.cooling == item.cooling:
			return True
	return False


# Creates new tasks when user wants to just preprocess data
def preprocess():
	task_queue = Queue()

	# Acquires information about tasks, that user want to compute
	tasks = general_dialogue("generate")
	if tasks == "home":
		return 1
	print("All options have been chosen! Checking for already computing tasks, please wait...")

	for item in tasks["dataset_target"]:
		for desc in tasks["descriptors"]:
			for frag in tasks["fragments"]:
				for perc in tasks["percentage"]:
					for split in tasks["splits"]:
						for scaling in tasks["scaling"]:
							for corr in tasks["correlation"]:
								task = Task()
								task.dataset = item[0]
								task.target = item[1]
								task.split = "s_" + split
								task.fragments = frag
								task.descriptor = desc
								task.percentage = perc
								task.scaling = scaling
								task.corr = corr
								task.script = "generate_data.sh"
								task_queue.put(task)

	# Filters out already assigned or computed tasks
	with generates_lock:
		for task in task_queue.queue:
			if (not comparator_in(task, waiting_generates.queue)) and (not comparator_in(task, computing_generates)):
				waiting_generates.put(task)

	print("Tasks were given to the task manager")
	return 1


def is_float(s):
	try:
		float(s)
		return True
	except ValueError:
		return False


def is_int(s):
	try:
		int(s)
		return True
	except ValueError:
		return False


# Handles the input of multiple floats
def gather_floats(container):
	numbers_splited = input().split()
	passed = False
	while (passed == False):
		passed = True
		if len(numbers_splited) == 0:
			passed = False
			print("Invalid input")
			numbers_splited = input().split()
			continue

		for number in numbers_splited:
			if number == "home":
				return "home"
			if not (is_float(number) or is_int(number)) or float(number) < 0:
				container.clear()
				passed = False
				print("Invalid input")
				numbers_splited = input().split()
			else:
				if number not in container:
					container.append(number)


# Handles the input of multiple ints
def gather_ints(container):
	numbers_splited = input().split()
	passed = False
	while (passed == False):
		passed = True
		if len(numbers_splited) == 0:
			passed = False
			print("Invalid input")
			numbers_splited = input().split()
			continue

		for number in numbers_splited:
			if number == "home":
				return "home"
			if not is_int(number) or int(number) < 0:
				container.clear()
				passed = False
				print("Invalid input")
				numbers_splited = input().split()
			else:
				if number not in container:
					container.append(number)


# The main dialogue which handle assignment of new tasks
def general_dialogue(what):
	methods = []
	dialogue = {}

	print()
	print("You can choose multiple options or write \"all\" to choose all.")
	print("Write \"home\" to get to the home menu")
	print("Which datasets do you want to use?")

	# Gets info about viable datasets to compute on server
	options, dict = datasets_on_server()

	for dataset in dict:
		print(dataset + ": " + str(dict[dataset]))

	choices = gather_input_target(options, dict)
	if choices == "home":
		return "home"

	# If user selects whole dataset instead of single target, assignes the whole dataset for computing
	dataset_target = []
	for item in choices:
		if item in dict:
			for target in dict[item]:
				dataset_target.append((item, target))
		else:
			for dataset in dict:
				if item in dict[dataset]:
					dataset_target.append((dataset, item))
					break

	print("Fragment type:")
	print("Choose from ecfp.1, ecfp.2, tt.2, tt.3, tt.4")
	options = ["ecfp.1", "ecfp.2", "tt.2", "tt.3", "tt.4"]
	fragments = gather_input(options)
	if fragments == "home":
		return "home"

	print("Descriptors:")
	print("Choose from RDKit or PaDEL")
	options = ["RDKit", "PaDEL"]
	descriptors = gather_input(options)
	if descriptors == "home":
		return "home"

	options.clear()
	if what == "screening":
		scaling = []
		print("Similarity function:")
		print("Choose from simple, euclidean, manhattan or nfrag version (for example euclidean100)")
		for i in range(0, 101):
			options.append("simple" + str(i))
			options.append("euclidean" + str(i))
			options.append("manhattan" + str(i))
		options.append("simple")
		options.append("euclidean")
		options.append("manhattan")
		methods = gather_input(options)
		if methods == "home":
			return "home"

		simple_preproc = False
		scaled_preproc = False
		for method in methods:
			if "simple" in method:
				simple_preproc = True
			if ("euclidean" in method) or ("manhattan" in method):
				scaled_preproc = True

		if simple_preproc:
			scaling.append("simple")

		if scaled_preproc:
			scaling.append("scaled")

	options.clear()
	if what == "generate":
		print("Similarity function:")
		print("Choose from binned or scaled")
		options.append("simple")
		options.append("scaled")
		scaling = gather_input(options)
		if scaling == "home":
			return "home"

	print("Fragment noise reduction treshold:")
	print("Write n, where n is a number between 0 and 100 (%)")
	options = []
	for i in range(0, 101):
		options.append(str(i))
	percentage = gather_input(options)
	if percentage == "home":
		return "home"

	options.clear()
	print("Correlation treshold:")
	print("Choose from 0 to 1")
	for i in range(1, 100):
		options.append(str(float(i) / 100))
		options.append(str(1))
		options.append(str(0))
	corr = gather_input(options)
	if corr == "home":
		return "home"

	options.clear()
	temperatures = ["0"]
	coolings = ["0"]
	distances = ["0"]
	parts = ["0"]
	annealing_types = []

	if what == "screening":
		print("Simmulated annealing:")
		print("Choose from none, rand_start, const_start")
		options = ["none", "rand_start", "const_start"]
		annealing_types = gather_input(options)
		if annealing_types == "home":
			return "home"

		if len(annealing_types) > 1 or annealing_types[0] != "none":
			print("Starting simmulated annealing temperature:")
			temperatures = []
			home = gather_floats(temperatures)
			if home == "home":
				return "home"

			print("Cooling rate:")
			coolings = []
			home = gather_floats(coolings)
			if home == "home":
				return "home"

			print("Annealing neighbour distance (% of initial value):")
			distances = []
			home = gather_floats(distances)
			if home == "home":
				return "home"

			print("Number of vector dimensions to change:")
			parts = []
			home = gather_ints(parts)
			if home == "home":
				return "home"

	options = []
	for i in range(1, splits_num[0] + 1):
		if i < 10:
			options.append("00" + str(i))
		if i < 100 and i > 9:
			options.append("0" + str(i))
		if i < 1000 and i > 99:
			options.append(str(i))

	print("Splits:")
	print(options)
	splits = gather_input(options)
	if splits == "home":
		return "home"

	dialogue["dataset_target"] = dataset_target
	dialogue["fragments"] = fragments
	dialogue["descriptors"] = descriptors
	dialogue["percentage"] = percentage
	dialogue["methods"] = methods
	dialogue["splits"] = splits
	dialogue["scaling"] = scaling
	dialogue["correlation"] = corr
	dialogue["temperatures"] = temperatures
	dialogue["distances"] = distances
	dialogue["annealing_types"] = annealing_types
	dialogue["parts"] = parts
	dialogue["coolings"] = coolings

	return dialogue


# Creates new tasks when user wants to run complete virtual screening
def virtual_screening():
	task_queue = Queue()
	filtered = Queue()
	tasks = general_dialogue("screening")
	if tasks == "home":
		return 1

	print("All options have been chosen! Checking for already computing tasks, please wait...")

	# Tasks to preprocesses data, for the computation
	for item in tasks["dataset_target"]:
		for desc in tasks["descriptors"]:
			for frag in tasks["fragments"]:
				for perc in tasks["percentage"]:
					for split in tasks["splits"]:
						for scaling in tasks["scaling"]:
							for corr in tasks["correlation"]:
								task = Task()
								task.dataset = item[0]
								task.target = item[1]
								task.split = "s_" + split
								task.fragments = frag
								task.descriptor = desc
								task.percentage = perc
								task.scaling = scaling
								task.corr = corr
								task.script = "generate_data.sh"
								task_queue.put(task)

	# Filters out already assigned or computed tasks
	with generates_lock:
		for task in task_queue.queue:
			if (not comparator_in(task, waiting_generates.queue)) and (not comparator_in(task, computing_generates)):
				waiting_generates.put(task)

	task_queue.queue.clear()
	filtered.queue.clear()

	# Tasks to screen the data
	for item in tasks["dataset_target"]:
		for desc in tasks["descriptors"]:
			for frag in tasks["fragments"]:
				for perc in tasks["percentage"]:
					for method in tasks["methods"]:
						for split in tasks["splits"]:
							for corr in tasks["correlation"]:
								for annealing_type in tasks["annealing_types"]:
									for temp in tasks["temperatures"]:
										for dist in tasks["distances"]:
											for part in tasks["parts"]:
												for cooling in tasks["coolings"]:
													task = Task()
													task.dataset = item[0]
													task.target = item[1]
													task.fragments = frag
													task.descriptor = desc
													task.percentage = perc
													task.method = method
													if method == "simple":
														task.scaling = "simple"
													else:
														task.scaling = "scaled"
													task.script = "screening.exe"
													task.split = "s_" + split
													task.corr = corr
													task.annealing_type = annealing_type
													if task.annealing_type != "none":
														task.distance = float(dist)
														task.orig_T = float(temp)
														task.parts = int(part)
														task.cooling = float(cooling);
													task_queue.put(task)

	# Filters out already assigned or computed tasks
	with screenings_lock:
		for task in task_queue.queue:
			if (not comparator_in(task, waiting_screenings.queue)) and (not comparator_in(task, computing_screenings)):
				waiting_screenings.put(task)
				# Create directory for the result
				final_dir = task_directory(task)
				run_subprocess("mkdir -p " + final_dir, 1)

	print("Tasks were given to the task manager")

	return 1


# Aggregates results from directory system to a .csv files
def get_results():
	run_subprocess("rm -r " + local_directory[0] + "/Results/Results_csv/ ; " + "mkdir -p " + local_directory[
		0] + "/Results/Results_csv/", 1)

	d = local_directory[0] + "/Results"
	dataset_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
	for dataset in dataset_list:
		d = dataset
		target_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
		for target in target_list:
			d = target
			fragment_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
			for fragment in fragment_list:
				d = fragment
				descriptor_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
				for descriptor in descriptor_list:
					d = descriptor
					method_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
					for method in method_list:
						d = method
						corr_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
						for corr in corr_list:
							d = corr
							noise_list = [os.path.join(d, o) for o in os.listdir(d) if
										  os.path.isdir(os.path.join(d, o))]
							for noise in noise_list:
								d = noise
								ann_list = [os.path.join(d, o) for o in os.listdir(d) if
											os.path.isdir(os.path.join(d, o))]
								for type in ann_list:
									d = type
									T_list = [os.path.join(d, o) for o in os.listdir(d) if
											  os.path.isdir(os.path.join(d, o))]
									for T in T_list:
										d = T
										cooling_list = [os.path.join(d, o) for o in os.listdir(d) if
														os.path.isdir(os.path.join(d, o))]
										for cooling in cooling_list:
											d = cooling
											dist_list = [os.path.join(d, o) for o in os.listdir(d) if
														 os.path.isdir(os.path.join(d, o))]
											for dist in dist_list:
												d = dist
												parts_list = [os.path.join(d, o) for o in os.listdir(d) if
															  os.path.isdir(os.path.join(d, o))]
												for parts in parts_list:
													# Ještě přidat rozdělení na none, local a teploty atd.
													d = parts
													split_list = [os.path.join(d, o) for o in os.listdir(d) if
																  os.path.isfile(os.path.join(d, o))]
													dataset_name = re.sub(local_directory[0] + "/Results" + "/", "",
																		  dataset)
													target_name = re.sub(dataset + "/", "", target)
													fragment_name = re.sub(target + "/", "", fragment)
													descriptor_name = re.sub(fragment + "/", "", descriptor)
													method_name = re.sub(descriptor + "/", "", method)
													corr_name = re.sub(method + "/", "", corr)
													noise_name = re.sub(corr + "/", "", noise)
													type_name = re.sub(noise + "/", "", type)
													T_name = re.sub(type + "/", "", T)
													cooling_name = re.sub(T + "/", "", cooling)
													dist_name = re.sub(cooling + "/", "", dist)
													parts_name = re.sub(dist + "/", "", parts)

													file_name = local_directory[
																	0] + "/Results/Results_csv/" + type_name + "_" + fragment_name + "_" + descriptor_name + "_" + method_name + "_" + corr_name + "_" + noise_name + "_" + T_name + "_" + cooling_name + "_" + dist_name + "_" + parts_name + ".csv"
													for split in split_list:
														with open(file_name, 'a+') as f:
															with open(split, 'r') as s:
																weights_names = s.readline().split()
																weights_values = s.readline().split()
																train_line = s.readline().split()
																test_line = s.readline().split()
																try:
																	# One line contains dataset name, target name, split, train AUC and test AUC
																	f.write(
																		"\"" + dataset_name + "\"," + "\"" + target_name + "\"," + "\"" + re.sub(
																			noise + "/", "", split) + "\"," + "\"" +
																		train_line[1] + "\"," + "\"" + train_line[
																			3] + "\"," + "\"" + train_line[
																			5] + "\"," + "\"" + train_line[
																			7] + "\"," + "\"" + train_line[
																			9] + "\"," + "\"" + test_line[
																			1] + "\"," + "\"" + test_line[
																			3] + "\"," + "\"" + test_line[
																			5] + "\"," + "\"" + test_line[
																			7] + "\"," + "\"" + test_line[9] + "\"\n")
																except:
																	f.write(
																		"\"" + dataset_name + "\"," + "\"" + target_name + "\"," + "\"" +
																		"Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"," + "\"" + "Error" + "\"\n")
																	print(split + " - computation not succesful")
															f.close()

	d = local_directory[0] + "/Results/Results_csv"
	result_list = [os.path.join(d, o) for o in os.listdir(d) if os.path.isfile(os.path.join(d, o))]
	target_dict = OrderedDict()
	for result in result_list:
		target_dict.clear()
		with open(result, 'r+') as r:
			for line in r:
				line = line.strip().split(",")
				if (line[1] not in target_dict) and line[3] != "\"Error\"":
					target_dict[line[1]] = {"train_before": [], "train_after": [], "train_random": [], "train_desc": [],
											"train_frag": [], "test_before": [], "test_after": [], "test_random": [],
											"test_desc": [], "test_frag": []}
				# Leaves out Error lines
				try:
					target_dict[line[1]]["train_before"].append(float((re.sub("\"", "", line[3])).strip()))
					target_dict[line[1]]["train_after"].append(float((re.sub("\"", "", line[4])).strip()))
					target_dict[line[1]]["train_random"].append(float((re.sub("\"", "", line[5])).strip()))
					target_dict[line[1]]["train_desc"].append(float((re.sub("\"", "", line[6])).strip()))
					target_dict[line[1]]["train_frag"].append(float((re.sub("\"", "", line[7])).strip()))
					target_dict[line[1]]["test_before"].append(float((re.sub("\"", "", line[8])).strip()))
					target_dict[line[1]]["test_after"].append(float((re.sub("\"", "", line[9])).strip()))
					target_dict[line[1]]["test_random"].append(float((re.sub("\"", "", line[10])).strip()))
					target_dict[line[1]]["test_desc"].append(float((re.sub("\"", "", line[11])).strip()))
					target_dict[line[1]]["test_frag"].append(float((re.sub("\"", "", line[12])).strip()))
				except:
					pass
			r.close()

		with open(result, 'a+') as r:
			r.write("\n")
			r.write(
				"TARGET" + "," + "\"" + "TRAIN_WITHOUT_WEIGHTS" + "\"," + "\"" + "TRAIN_WITH_WEIGHTS" + "\"," + "\"" + "TRAIN_RANDOM_WEIGHTS" + "\"," + "\"" + "TRAIN_DESCRIPTORS" + "\"," + "\"" + "TRAIN_FRAGMENTS" + "\"," + "\"" + "TEST_WITHOUT_WEIGHTS" + "\"," + "\"" + "TEST_WITH_WEIGHTS" + "\"," + "\"" + "TEST_RANDOM_WEIGHTS" + "\"," + "\"" + "TEST_DESCRIPTORS" + "\"," + "\"" + "TEST_FRAGMENTS" + "\"\n")
			average_train_before = 0.0
			average_train_after = 0.0
			average_train_random = 0.0
			average_train_desc = 0.0
			average_train_frag = 0.0
			average_test_before = 0.0
			average_test_after = 0.0
			average_test_random = 0.0
			average_test_desc = 0.0
			average_test_frag = 0.0
			for target in target_dict:
				train_num_before = aggregate(target_dict, target, "train_before")
				train_num_after = aggregate(target_dict, target, "train_after")
				train_num_random = aggregate(target_dict, target, "train_random")
				train_num_desc = aggregate(target_dict, target, "train_desc")
				train_num_frag = aggregate(target_dict, target, "train_frag")
				test_num_before = aggregate(target_dict, target, "test_before")
				test_num_after = aggregate(target_dict, target, "test_after")
				test_num_random = aggregate(target_dict, target, "test_random")
				test_num_desc = aggregate(target_dict, target, "test_desc")
				test_num_frag = aggregate(target_dict, target, "test_frag")

				average_train_before += train_num_before
				average_train_after += train_num_after
				average_train_random += train_num_random
				average_train_desc += train_num_desc
				average_train_frag += train_num_frag
				average_test_before += test_num_before
				average_test_after += test_num_after
				average_test_random += test_num_random
				average_test_desc += test_num_desc
				average_test_frag += test_num_frag

				r.write(target + "," + "\"" + str(train_num_before) + "\"," + "\"" + str(
					train_num_after) + "\"," + "\"" + str(train_num_random) + "\"," + "\"" + str(
					train_num_desc) + "\"," + "\"" + str(train_num_frag) + "\"," + "\"" + str(
					test_num_before) + "\"," + "\"" + str(test_num_after) + "\"," + "\"" + str(
					test_num_random) + "\"," + "\"" + str(test_num_desc) + "\"," + "\"" + str(test_num_frag) + "\"\n")
			# Handles empty
			if len(target_dict) != 0:
				average_train_before /= len(target_dict)
				average_train_after /= len(target_dict)
				average_train_random /= len(target_dict)
				average_train_desc /= len(target_dict)
				average_train_frag /= len(target_dict)
				average_test_before /= len(target_dict)
				average_test_after /= len(target_dict)
				average_test_random /= len(target_dict)
				average_test_desc /= len(target_dict)
				average_test_frag /= len(target_dict)
			r.write("" + "," + "\"" + str(average_train_before) + "\"," + "\"" + str(
				average_train_after) + "\"," + "\"" + str(average_train_random) + "\"," + "\"" + str(
				average_train_desc) + "\"," + "\"" + str(average_train_frag) + "\"," + "\"" + str(
				average_test_before) + "\"," + "\"" + str(average_test_after) + "\"," + "\"" + str(
				average_test_random) + "\"," + "\"" + str(average_test_desc) + "\"," + "\"" + str(
				average_test_frag) + "\"\n")

	return 1


def aggregate(target_dict, target, type):
	total = 0
	for num in target_dict[target][type]:
		total += num
	total /= len(target_dict[target][type])
	return total


# Prints information about cluster
def cluster_info():
	alive = 0
	started = 0
	for comp in cluster_config:
		if comp.alive == True:
			alive = alive + 1
		if comp.started == True:
			started = started + 1

	print("Alive: " + str(alive))
	print("Started :" + str(started))
	return 1


# Prints information about running computation
def process_info():
	print("Waiting generates: " + str(waiting_generates.qsize()))
	print("Computing generates: " + str(len(computing_generates)))
	print("Waiting screenings: " + str(waiting_screenings.qsize()))
	print("Computing screenings: " + str(len(computing_screenings)))
	print()

	return 1


# Sends data to all cluster computers
def update(directory):
	if location_info[0] == "local":
		print("You can't use this function in local mode.")
		return 1

	with cluster_lock:
		print("Zipping " + directory)
		run_subprocess("cd " + local_directory[0] + "; zip -r " + directory + ".zip " + directory, 10000)
		print()

		for comp in cluster_config:
			if comp.alive == True:
				print(comp.name + " deleting old " + directory)
				run_subprocess(
					"sshpass -p \"" + log_pass[1] + "\" ssh -o StrictHostKeyChecking=no \"" + log_pass[
						0] + "@" + comp.name + "\" \"rm -r " + cluster_directory[0] + "/" + directory + "\"", 10000)
		print()

		for comp in cluster_config:
			if comp.alive == True:
				print(comp.name + " sending zip")
				run_subprocess(
					"sshpass -p \"" + log_pass[1] + "\" scp " + local_directory[0] + "/" + directory + ".zip" + " " +
					log_pass[
						0] + "@" + comp.name + ":" + cluster_directory[0], 10000)
		print()

		for comp in cluster_config:
			if comp.alive == True:
				print(comp.name + " unzipping")
				run_subprocess(
					"sshpass -p \"" + log_pass[1] + "\" ssh -o StrictHostKeyChecking=no \"" + log_pass[
						0] + "@" + comp.name + "\" \"unzip -o " + cluster_directory[0] + "/" + directory + ".zip -d " +
					cluster_directory[0] + "\"", 10000)
		print()

		for comp in cluster_config:
			if comp.alive == True:
				print(comp.name + " deleting zip from cluster")
				run_subprocess(
					"sshpass -p \"" + log_pass[1] + "\" ssh -o StrictHostKeyChecking=no \"" + log_pass[
						0] + "@" + comp.name + "\" \"rm " + cluster_directory[0] + "/" + directory + ".zip\"", 10000)
		print()

		print("Deleting local zip")
		run_subprocess("rm " + local_directory[0] + "/" + directory + ".zip", 10000)

	return 1


# Kills all servers and closes the application
def close():
	if location_info[0] == "cluster" and not (not log_pass):
		for comp in cluster_config:
			run_subprocess("sshpass -p \"" + log_pass[1] + "\" ssh -o StrictHostKeyChecking=no " + log_pass[
				0] + "@" + comp.name + " kill -9 -1", 2)
			print(comp.name + " closed")
	return 0


def wrong_answer():
	print("You chose invalid input")
	return 1


# Executes choice from dialogue
def execute_choice(argument):
	if not (str(argument) in dialogue_switcher):
		func = wrong_answer()
	else:
		choice = dialogue_switcher[str(argument)]
		if len(choice) == 1:
			func = choice[0]()
		else:
			# If parameter is needed
			func = choice[0](choice[1])
	return func


def run_dialogue():
	print("Write a number to choose one of the following")
	print()
	print("[1] Preprocess datasets")
	print("[2] Virtual screening")
	print("[3] Get results")
	print("[4] Cluster info")
	print("[5] Computations info")
	print("[6] Update Code_config")
	print("[7] Update Data")
	print("[8] Update Preprocessed_tmp")
	print("[9] Update Preprocessed")
	print("[10] Set number of tasks per computer")
	print("[11] Set the seed for random number generation")
	print("[12] Close application")
	argument = input()
	return execute_choice(argument)


# Sets the maximum number of tasks which a cluster computer can compute
def max_tasks_set():
	print("Set number of tasks which will be computed on computer.")
	print("When calculating ecfp.2, we do not recommend more than 1 task.")
	print("Overall we do not recommend more than 8 tasks per computer.")
	print("Now you are computing " + str(max_tasks[0]) + " tasks per computer")
	passed = False
	nums = []
	while passed == False:
		gather_ints(nums)
		if len(nums) > 1:
			print("Invalid input!")
			nums = []
		else:
			max_tasks[0] = int(nums[0])
			passed = True

	return 1


# Sets seed for random number generation (used in screening)
def seed_set():
	print("Set the seed")
	print("Now you are using " + str(seed[0]) + " as seed")
	passed = False
	nums = []
	while passed == False:
		gather_floats(nums)
		if len(nums) > 1:
			print("Invalid input!")
			nums = []
		else:
			seed[0] = int(nums[0])
			passed = True

	return 1


def create_directory():
	for comp in cluster_config:
		if comp.alive == True:
			print(comp.name + " creating directory " + cluster_directory[0])
			run_subprocess(
				"sshpass -p \"" + log_pass[1] + "\" ssh -o StrictHostKeyChecking=no \"" + log_pass[
					0] + "@" + comp.name + "\" \"mkdir -p " + cluster_directory[0] + "\"", 10000)


def location():
	print("Local run, use cluster or setup cluster (when used first) [local/cluster/setup]:")
	s = input()

	while (True):
		if s == "local":
			location_info.append("local")
			comp = Computer()
			comp.name = "local"
			comp.adress = "local"
			comp.alive = True
			comp.started = True
			comp.connecting = False
			comp.free = True
			cluster_config.append(comp)
			break
		if s == "cluster":
			print("Starting cluster")
			location_info.append("cluster")
			# Reads cluster configuration file - returns list of comp classes, containg adress and name
			read_cluster_config()
			# Acquires login and password for cluster and checks which computers are online, free and wich server scripts started
			cluster_refresh()
			# Information about cluster displayed to the user
			cluster_info()
			break
		if s == "setup":
			print("Setting up cluster")
			location_info.append("cluster")
			# Reads cluster configuration file - returns list of comp classes, containg adress and name
			read_cluster_config()

			# Acquires login and password for cluster
			setup_login()

			create_directory()

			update("Code_config")
			print("Cluster is now setup without any data")
			print()

			cluster_refresh()
			# Information about cluster displayed to the user
			cluster_info()
			print("Now you can send preprocessed or raw data to cluster for screening.")
			break
		print("Invalid input!")
		s = input()


def _read_configuration():
	parser = argparse.ArgumentParser(
		description='(Somename) run configuration')
	parser.add_argument('-atr', type=str, dest='active_train',
						help='active_train molecules .sdf',
						required=False)
	parser.add_argument('-itr', type=str, dest='inactive_train',
						help='inactive_train molecules .sdf',
						required=False)
	parser.add_argument('-ava', type=str, dest='active_validation',
						help='active_validation molecules .sdf',
						required=False)
	parser.add_argument('-iva', type=str, dest='inactive_validation',
						help='inactive_validation molecules .sdf',
						required=False)
	parser.add_argument('-ate', type=str, dest='active_test',
						help='active_test molecules .sdf',
						required=False)
	parser.add_argument('-ite', type=str, dest='inactive_test',
						help='inactive_test molecules .sdf',
						required=False)
	parser.add_argument('-w', type=str, dest='weights',
						help='file with weights',
						required=False)
	parser.add_argument('-o', type=str, dest='output',
						help='output file',
						required=False)
	parser.add_argument('-tmp', type=str, dest='tmp',
						help='tmp directory required for computtation',
						required=False)

	parser.add_argument('-f', type=str, dest='fragment_type',
						help='Type of fragments calculated (recommend ecfp.1)',
						required=False)
	parser.add_argument('-p', type=str, dest='fragment_percentage',
						help='Treshold percentage for fragment elimination (recommend 35)',
						required=False)
	parser.add_argument('-d', type=str, dest='descriptor',
						help='Descriptors used (recommend RDKit)',
						required=False)
	parser.add_argument('-sb', type=str, dest='scaling_binning',
						help='Scalling or binning of descriptors (recommend scaling)',
						required=False)
	parser.add_argument('-corr', type=str, dest='correlation',
						help='Correlation treshold for elimination of descriptors (recommend 0.3)',
						required=False)

	parser.add_argument('-m', type=str, dest='method',
						help='Comparison method used (recommend euclidean100)',
						required=False)
	parser.add_argument('-a', type=str, dest='annealing_type',
						help='Type of annealing used (none, rand_start or const_start)',
						required=False)
	parser.add_argument('-T', type=str, dest='temperature',
						help='Starting temperature (recommend 1.2)',
						required=False)
	parser.add_argument('-dist', type=str, dest='distance',
						help='Distance of changed descriptors in percent (recommend 50)',
						required=False)
	parser.add_argument('-parts', type=str, dest='parts',
						help='How many parts of weights to change (recommend 1)',
						required=False)
	parser.add_argument('-c', type=str, dest='cooling',
						help='How fast to cool annealing (recommend 0.999)',
						required=False)
	parser.add_argument('-seed', type=str, dest='seed',
						help='Seed used when generating random numbers',
						required=False)
	parser.add_argument('-type', type=str, dest='type',
						help='Type of computation',
						required=True)

	return vars(parser.parse_args())


# Hanldes the creation of a model when command line is used
def phase_1(configuration):
	# Check the parameters
	if ((configuration["active_train"] is None) or (configuration["inactive_train"] is None) or (
				configuration["output"] is None) or (configuration["tmp"] is None)):
		print("Model creation requires: -atr, -itr, -o, -tmp")
		print("Your are missing some parameters")
		print(configuration)
		return 0

	# Default values
	if (configuration["annealing_type"] is None):
		configuration["annealing_type"] = "const_start"
	if (configuration["fragment_type"] is None):
		configuration["fragment_type"] = "ecfp.1"
	if (configuration["fragment_percentage"] is None):
		configuration["fragment_percentage"] = "100"
	if (configuration["descriptor"] is None):
		configuration["descriptor"] = "RDKit"
	if (configuration["scaling_binning"] is None):
		configuration["scaling_binning"] = "scaling"
	if (configuration["correlation"] is None):
		configuration["correlation"] = "1"
	if (configuration["method"] is None):
		configuration["method"] = "euclidean100"

	if (configuration["fragment_type"] not in ["ecfp.1", "ecfp.2", "tt.2", "tt.3", "tt.4"]):
		print("-f can be only ecfp.1, ecfp.2, tt.2, tt.3 or tt.4")
		return 0

	options = []
	for i in range(0, 101):
		options.append(str(i))
	if (configuration["fragment_percentage"] not in options):
		print("-p can be only from 0 to 100")
		return 0

	if (configuration["descriptor"] not in ["RDKit", "PaDEL"]):
		print("-d can be only RDKit or PaDEL")
		return 0

	if (configuration["scaling_binning"] not in ["scaling", "binning"]):
		print("-sb can be only scaling or binning")
		return 0

	options = []
	for i in range(1, 100):
		options.append(str(float(i) / 100))
		options.append(str(1))
		options.append(str(0))
	if (configuration["correlation"] not in options):
		print("-corr can be only from 0 to 1")
		return 0

	options = []
	for i in range(0, 101):
		options.append("simple" + str(i))
		options.append("euclidean" + str(i))
		options.append("manhattan" + str(i))
	options.append("simple")
	options.append("euclidean")
	options.append("manhattan")
	if (configuration["method"] not in options):
		print("-m can be only euclidean, manhattan, simple or their nfrag version")
		return 0

	if (configuration["annealing_type"] not in ["none", "rand_start", "const_start"]):
		print("-a can be only none, rand_start or const_start")
		return 0

	# These options are not required but can't be None
	if configuration["temperature"] is None:
		configuration["temperature"] = str(1000)
	if configuration["distance"] is None:
		configuration["distance"] = str(25)
	if configuration["parts"] is None:
		configuration["parts"] = str(1)
	if configuration["cooling"] is None:
		configuration["cooling"] = str(0.999)
	if configuration["seed"] is None:
		configuration["seed"] = "RANDOM"
	if configuration["seed"] != "RANDOM":
		if not (is_float(configuration["seed"]) or is_int(configuration["seed"])):
			print("-seed must be a float or string RANDOM")
			return 0

	if not (is_float(configuration["temperature"]) or is_int(configuration["temperature"])) or float(
			configuration["temperature"]) < 0:
		print("-t must be positive float number")
		return 0

	if not (is_float(configuration["distance"]) or is_int(configuration["distance"])) or float(
			configuration["distance"]) < 0:
		print("-d must be positive float number")
		return 0

	if not is_int(configuration["parts"]) or int(configuration["parts"]) < 0:
		print("-p must be positive integer number")
		return 0

	if not (is_float(configuration["cooling"]) or is_int(configuration["cooling"])) or float(
			configuration["cooling"]) < 0:
		print("-c must be positive float number")
		return 0

	if configuration["active_validation"] is None:
		configuration["active_validation"] = "dummy"

	if configuration["inactive_validation"] is None:
		configuration["inactive_validation"] = "dummy"

	# Parameters for the calculation
	params = "bash " + local_directory[0] + "/Code_config/cmd_line_run.sh" + " " + configuration[
		"active_train"] + " " + configuration["inactive_train"] + " " + configuration[
				 "active_validation"] + " " + configuration[
				 "inactive_validation"] + " " + "active_test_dummy" + " " + "inactive_test_dummy" + " " + "weights_dummy" + " " + "output_dummy" + " " + \
			 configuration["tmp"] + " " + configuration["fragment_type"] + " " + configuration[
				 "fragment_percentage"] + " " + configuration["descriptor"] + " " + configuration[
				 "scaling_binning"] + " " + configuration["correlation"] + " " + configuration["method"] + " " + \
			 configuration["annealing_type"] + " " + configuration["temperature"] + " " + configuration[
				 "distance"] + " " + configuration["parts"] + " " + configuration["cooling"] + " " + \
			 configuration["type"] + " " + local_directory[0] + " " + configuration["seed"]

	# Run the calculation
	response, err = run_subprocess(params, 5000)

	with open(configuration["output"], "a+") as output:
		output.write(response.replace('\\n', '\n'))


# Hanldes the testing of a model when command line is used
def phase_2(configuration):
	# Check the parameters
	if ((configuration["active_train"] is None) or (configuration["active_test"] is None) or (
				configuration["inactive_test"] is None) or (configuration["output"] is None) or (
				configuration["tmp"] is None) or (configuration["weights"] is None)):
		print("Model testing requires: -atr, -ate, -ite, -o, -tmp, -w")
		print("Your are missing some parameters")
		return 0
	if (configuration["fragment_type"] is None):
		configuration["fragment_type"] = "ecfp.1"
	if (configuration["fragment_percentage"] is None):
		configuration["fragment_percentage"] = "100"
	if (configuration["descriptor"] is None):
		configuration["descriptor"] = "RDKit"
	if (configuration["scaling_binning"] is None):
		configuration["scaling_binning"] = "scaling"
	if (configuration["correlation"] is None):
		configuration["correlation"] = "1"
	if (configuration["method"] is None):
		configuration["method"] = "euclidean100"

	if (configuration["fragment_type"] not in ["ecfp.1", "ecfp.2", "tt.2", "tt.3", "tt.4"]):
		print("-f can be only ecfp.1, ecfp.2, tt.2, tt.3 or tt.4")
		return 0

	options = []
	for i in range(0, 101):
		options.append(str(i))
	if (configuration["fragment_percentage"] not in options):
		print("-p can be only from 0 to 100")
		return 0

	if (configuration["descriptor"] not in ["RDKit", "PaDEL"]):
		print("-d can be only RDKit or PaDEL")
		return 0

	if (configuration["scaling_binning"] not in ["scaling", "binning"]):
		print("-sb can be only scaling or binning")
		return 0

	options = []
	for i in range(0, 101):
		options.append("simple" + str(i))
		options.append("euclidean" + str(i))
		options.append("manhattan" + str(i))
	options.append("simple")
	options.append("euclidean")
	options.append("manhattan")
	if (configuration["method"] not in options):
		print("-m can be only euclidean, manhattan, simple or their nfrag version")
		return 0

	if configuration["active_validation"] is None:
		configuration["active_validation"] = "dummy"

	if configuration["seed"] is None:
		configuration["seed"] = "RANDOM"

	if configuration["seed"] != "RANDOM":
		if not (is_float(configuration["seed"]) or is_int(configuration["seed"])):
			print("-seed must be a float or string RANDOM")
			return 0

	# Parameters for the calculation
	params = "bash " + local_directory[0] + "/Code_config/cmd_line_run.sh" + " " + configuration[
		"active_train"] + " " + "inactive_train_dummy" + " " + configuration[
				 "active_validation"] + " " + "inactive_validation_dummy" + " " + configuration["active_test"] + " " + \
			 configuration["inactive_test"] + " " + configuration["weights"] + " " + "output_dummy" + " " + \
			 configuration["tmp"] + " " + configuration["fragment_type"] + " " + configuration[
				 "fragment_percentage"] + " " + configuration["descriptor"] + " " + configuration[
				 "scaling_binning"] + " " + "0" + " " + configuration[
				 "method"] + " " + "annealing_type_dummy" + " " + "0" + " " + "0" + " " + "0" + " " + "0" + " " + \
			 configuration["type"] + " " + local_directory[0] + " " + configuration["seed"]

	# Run the calculation
	response, err = run_subprocess(params, 5000)

	with open(configuration["output"], "a+") as output:
		output.write(response.replace('\\n', '\n'))


def _main():
	# Read command line input
	configuration = _read_configuration()

	# Read configuration file and assign paths
	directory = os.path.dirname(os.path.abspath(__file__))

	for c in directory:
		if c.isspace():
			print("Please remove white spaces from directory path Wefrag.")
			exit()
		if '\"' in c:
			print("Please remove \" from directory path to Wefrag.")
			exit()
		if '\'' in c:
			print("Please remove \' from directory path Wefrag.")
			exit()
		if '`' in c:
			print("Please remove ` from directory path Wefrag.")
			exit()
		if '$' in c:
			print("Please remove $ from directory path Wefrag.")
			exit()

	with open(directory + "/cluster_config.txt", 'r') as stream:
		cluster_dir_splits = stream.readline().split()
	try:
		cluster_directory.append(cluster_dir_splits[1])
		splits_num.append(int(cluster_dir_splits[3]))
		seed.append(cluster_dir_splits[5])
	except:
		print("Missing or wrong configuration information.")
		print(
			"First line needs to be: cluster_directory: directory_path splits_num: some_number seed: some_number_or_RANDOM")
		exit()
	if seed[0] != "RANDOM":
		if not (is_float(seed[0]) or is_int(seed[0])):
			print("seed must be a float or string RANDOM")
			exit()

	path = re.sub('/Code_config$', '', directory)
	local_directory.append(path)

	if (configuration["type"] == "model" or configuration["type"] == "phase_1"):
		configuration["type"] = "model"
		phase_1(configuration)
	else:
		if (configuration["type"] == "test" or configuration["type"] == "phase_2"):
			configuration["type"] = "test"
			phase_2(configuration)
		else:
			if (configuration["type"] == "menu"):
				print("=================")
				print("Welcome to WeFrag")
				print("=================")
				print()

				location()
				create_thread(task_manager, [])
				dialogue_result = 1
				# Dialog loop for task assignment
				while dialogue_result == 1:
					dialogue_result = run_dialogue()
			else:
				print("-type parameter must be eiter: phase_1, phase_2 or menu")
	exit()


# These variables need to be global, multiple threads are accessing them
log_pass = []

splits_num = []
seed = []

local_directory = []
cluster_directory = []
location_info = []

cluster_lock = threading.Lock()
cluster_config = []

generates_lock = threading.Lock()
waiting_generates = Queue()
computing_generates = []

screenings_lock = threading.Lock()
waiting_screenings = Queue()
computing_screenings = []

port = 1984
max_tasks = [1]
send_lock = threading.Lock()

dialogue_switcher = {
	"1": [preprocess],
	"2": [virtual_screening],
	"3": [get_results],
	"4": [cluster_info],
	"5": [process_info],
	"6": [update, "Code_config"],
	"7": [update, "Data"],
	"8": [update, "Preprocessed_tmp"],
	"9": [update, "Preprocessed"],
	"10": [max_tasks_set],
	"11": [seed_set],
	"12": [close],
}
all_tasks = []

if __name__ == '__main__':
	_main()
