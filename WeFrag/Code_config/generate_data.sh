#!/usr/bin/env bash

# Handles data preprocessing when the directory system is used

dataset=$1      # dataset of data which we preprocess
target=$2       # target of data which we preprocess
fragment=$3     # type of fragments [ecfp.1, ecfp.2, tt.2, tt.3, tt.4]
treshold=$4     # fragment noise treshold percentage [0-100]
descriptors=$5  # descriptor type [RDKit, PaDEL]
split_dir=$6    # directory with the molecules of a split, which we preprocess
split=$7        # name of split which we preprocess
scaling=$8      # scaling or binning [scaling, binning]
corr=$9         # correlation treshold [0-1]

tmp="Preprocessed_tmp" # name of tmp directory
path=/$tmp/$dataset/$target/$fragment   # path to splits
dir=${10}
final_name="Preprocessed"
final_path=/$final_name/$dataset/$target/$fragment/$descriptors
final_dir=$dir$final_path/$scaling/"corr_"$corr/"noise_"$treshold # name for final directory for preprocessed data

# Generates data for screening, if the data doesn't already exists and creates directories for them

if ! [ -f $final_dir/$split ]; then

    mkdir -p $dir$path/$descriptors

        decoys_sdf=$dir/"Data"/$dataset/$target/$target"_Decoys.sdf"
        ligands_sdf=$dir/"Data"/$dataset/$target/$target"_Ligands.sdf"

        decoys_sdf_tmp=$dir/"Data"/$dataset/$target/$target"_Decoys_tmp.sdf"
        ligands_sdf_tmp=$dir/"Data"/$dataset/$target/$target"_Ligands_tmp.sdf"
        target_short=$target


        if ! [ -f $decoys_sdf ]; then

            # Merges all decoys from all splits into 1 file
            for D in `find $dir/"Data"/$dataset/$target/* -maxdepth 0 -type d`
            do
                split_dir=$D
                decoys_sdf_split=$split_dir/$target"_Decoys.sdf"
                if ! [ -f $decoys_sdf_split ]; then
                    python3 $dir/Code_config/merge_sdf.py -te $split_dir/$target_short"_decoys_test.sdf" -tr $split_dir/$target_short"_decoys_train.sdf" -va $split_dir/$target_short"_decoys_validation.sdf" -o $decoys_sdf_split
                    if [ $? -ne 0 ]; then
                        echo "Split decoys.sdf generation error - DO YOU HAVE DATA IN THE RIGHT DIRECTORY??"
                        exit
                    fi
                fi
            cat $decoys_sdf_split >> $decoys_sdf_tmp
            rm $decoys_sdf_split
            done

            #Remove duplicate molecules to reduce the size of .sdf
            python3 $dir/Code_config/remove_duplicates.py -i $decoys_sdf_tmp -o $decoys_sdf
            rm $decoys_sdf_tmp
        fi

        if ! [ -f $ligands_sdf ]; then

            # Merges all ligands from all splits into 1 file
            for D in `find $dir/"Data"/$dataset/$target/* -maxdepth 0 -type d`
            do
                split_dir=$D
                ligands_sdf_split=$split_dir/$target"_Ligands.sdf"
                if ! [ -f $ligands_sdf_split ]; then
                    python3 $dir/Code_config/merge_sdf.py -te $split_dir/$target_short"_actives_test.sdf" -tr $split_dir/$target_short"_actives_train.sdf" -va $split_dir/$target_short"_actives_validation.sdf" -o $ligands_sdf_split
                    if [ $? -ne 0 ]; then
                        echo "Split ligands.sdf generation error - DO YOU HAVE DATA IN THE RIGHT DIRECTORY??"
                        exit
                    fi
                fi
            cat $ligands_sdf_split >> $ligands_sdf_tmp
            rm $ligands_sdf_split
            done

            #Remove duplicate molecules to reduce the size of .sdf
            python3 $dir/Code_config/remove_duplicates.py -i $ligands_sdf_tmp -o $ligands_sdf
            rm $ligands_sdf_tmp
        fi

        split_dir=$dir/"Data"/$dataset/$target/$split

        # Creates info about split we are currently preprocessing
        if ! [ -f $split_dir/$split ]; then
            python3 $dir/Code_config/split_info.py -ted $split_dir/$target_short"_decoys_test.sdf" -trd $split_dir/$target_short"_decoys_train.sdf" -vad $split_dir/$target_short"_decoys_validation.sdf" -tel $split_dir/$target_short"_actives_test.sdf" -trl $split_dir/$target_short"_actives_train.sdf" -val $split_dir/$target_short"_actives_validation.sdf" -o $split_dir/$split
            if [ $? -ne 0 ]; then
                echo "Split info generation error - DO YOU HAVE DATA IN THE RIGHT DIRECTORY??"
                exit
            fi
        fi


    # Generates fragments for decoys
    if ! [ -f $dir$path/"decoys_fragments" ]; then
        python3 $dir/Code_config/extract_fragments.py -i $decoys_sdf -f $fragment -o $dir$path/"decoys_fragments"
        if [ $? -ne 0 ]; then
            echo "Decoys fragments generation error - DO YOU HAVE RDKIT ENABLED??"
            exit
        fi
    fi

    # Generates fragments for ligands
    if ! [ -f $dir$path/"ligands_fragments" ]; then
        python3 $dir/Code_config/extract_fragments.py -i $ligands_sdf -f $fragment -o $dir$path/"ligands_fragments"
        if [ $? -ne 0 ]; then
            echo "Ligands fragments generation error - DO YOU HAVE RDKIT ENABLED??"
            exit
        fi
    fi

    # generate PaDEL descriptors for fragments
    if [ "$descriptors" = "PaDEL" ]; then

            # Generates teomporary data for PaDEL descriptor generator
            if ! [ -f $dir$path/$descriptors/"ligands_desc_temp" ]; then
                python3 $dir/Code_config/padel_descriptors.py -i $dir$path/"ligands_fragments" -o $dir$path/$descriptors/"ligands_desc_temp" -f > /dev/null 2>&1
                if [ $? -ne 0 ]; then
                    echo "Ligands temp generation error - DO YOU HAVE PaDEL in Code_config??"
                    exit
                fi
            fi

            # Generates raw PaDEL descriptors for fragments
            if ! [ -f $dir$path/$descriptors/"ligands_desc_raw" ]; then
                java -jar $dir/Code_config/PaDel/PaDEL-Descriptor.jar -maxruntime 5000 -2d -dir $dir$path/$descriptors/"ligands_desc_temp" -file $dir$path/$descriptors/"ligands_desc_raw" > /dev/null 2>&1
                if [ $? -ne 0 ]; then
                    echo "Ligands raw desc generation error"
                    exit
                fi
            fi


            if ! [ -f $dir$path/$descriptors/"decoys_desc_temp" ]; then
                python3 $dir/Code_config/padel_descriptors.py -i $dir$path/"decoys_fragments" -o $dir$path/$descriptors/"decoys_desc_temp" -f > /dev/null 2>&1
                if [ $? -ne 0 ]; then
                    echo "Decoys temp generation error - DO YOU HAVE PaDEL in Code_config??"
                    exit
                fi
            fi

            if ! [ -f $dir$path/$descriptors/"decoys_desc_raw" ]; then
                java -jar $dir/Code_config/PaDel/PaDEL-Descriptor.jar -maxruntime 5000 -2d -dir $dir$path/$descriptors/"decoys_desc_temp" -file $dir$path/$descriptors/"decoys_desc_raw" > /dev/null 2>&1
                if [ $? -ne 0 ]; then
                    echo "Decoys raw desc generation error"
                    exit
                fi
            fi
    fi

    # generate RDKit descriptors for fragments
    if [ "$descriptors" = "RDKit" ]; then

            # Generates raw RDKit descriptors
            if ! [ -f $dir$path/$descriptors/"ligands_desc_raw" ]; then
                python3 $dir/Code_config/rdkit_descriptors.py -i $dir$path/"ligands_fragments" -o $dir$path/$descriptors/"ligands_desc_raw" --fragments
                if [ $? -ne 0 ]; then
                    echo "Ligands raw desc generation error - DO YOU HAVE RDKIT ENABLED??"
                    exit
                fi
            fi

            if ! [ -f $dir$path/$descriptors/"decoys_desc_raw" ]; then
                python3 $dir/Code_config/rdkit_descriptors.py -i $dir$path/"decoys_fragments" -o $dir$path/$descriptors/"decoys_desc_raw" --fragments
                if [ $? -ne 0 ]; then
                    echo "Decoys raw desc generation error - DO YOU HAVE RDKIT ENABLED??"
                    exit
                fi
            fi
    fi

    mkdir -p $final_dir

    # Throws away fragments over treshold, reindexes fragments in molecules, creates better format for reading and removes and correlated descriptors
    if ! [ -f $final_dir/$split"_phase_1" ]; then
        python3 $dir/Code_config/phase_preprocess.py -df $dir$path/"decoys_fragments" -lf $dir$path/"ligands_fragments" -s $split_dir/$split -t $treshold -c $corr -tmp True -p 1 -o $final_dir/$split"_phase_1" -dd $dir$path/$descriptors/"decoys_desc_raw" -ld $dir$path/$descriptors/"ligands_desc_raw" -m $scaling
        if [ $? -ne 0 ]; then
            echo "Phase preprocessing error phase 1"
            exit
        fi
    fi

    # Throws away fragments over treshold, reindexes fragments in molecules, creates better format for reading and removes and correlated descriptors
    if ! [ -f $final_dir/$split"_phase_2" ]; then
        python3 $dir/Code_config/phase_preprocess.py -df $dir$path/"decoys_fragments" -lf $dir$path/"ligands_fragments" -s $split_dir/$split -t $treshold -p 2 -w $final_dir/$split"_phase_1_desc_tmp" -o $final_dir/$split"_phase_2" -dd $dir$path/$descriptors/"decoys_desc_raw" -ld $dir$path/$descriptors/"ligands_desc_raw" -m $scaling
        if [ $? -ne 0 ]; then
            echo "Phase preprocessing error phase 2"
            exit
        fi
    fi

    rm $final_dir/$split"_phase_1_desc_tmp"

fi

echo "Done"
