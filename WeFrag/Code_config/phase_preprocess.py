#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import argparse
import csv
import numpy as np
import math

"""
Handles the removal of constart descriptors, correlated descriptors, descriptors not used in weights and fragment noise.
Scales or bins the descriptors.
Creates final data format

-df - decoys_fragments.json
-lf - decoys_fragments.json
-s - information about split
-t - fragment noise treshold
-o - output file
-p - phase of preprocessing (creation of model or testing)
-c - - correlation treshold
-w - file with weights
-tmp - tmp file with descriptors names
-dd - decoys descriptors (output of RDKit or PaDEL)
-ld - ligands descriptors (output of RDKit or PaDEL)
-m - scaling or binning method
"""


def _read_configuration():
	parser = argparse.ArgumentParser(
		description='Preprocessing of descriptor .csv file')
	parser.add_argument('-df', type=str, dest='dec_fragments',
						help='Input decoys fragments in json',
						required=True)
	parser.add_argument('-lf', type=str, dest='lig_fragments',
						help='Input ligands fragments in json',
						required=True)
	parser.add_argument('-s', type=str, dest='split',
						help='Split info',
						required=True)
	parser.add_argument('-t', type=str, dest='treshold',
						help='Treshold for percentage of fragments in molecules',
						required=True)
	parser.add_argument('-o', type=str, dest='final_output',
						help='Output file of preprocessed data',
						required=True)
	parser.add_argument('-p', type=int, dest='phase',
						help='For which phase are data preprocessed.',
						required=True)
	parser.add_argument('-c', type=str, dest='correlation',
						help='Correlation used',
						required=False)
	parser.add_argument('-w', type=str, dest='weights',
						help='Weights calculated in phase 1',
						required=False)
	parser.add_argument('-tmp', type=str, dest='tmp',
						help='To print tmp weights header',
						required=False)
	parser.add_argument('-dd', type=str, dest='decoys_desc_raw',
						help='Input raw decoys descriptors',
						required=True)
	parser.add_argument('-ld', type=str, dest='ligands_desc_raw',
						help='Input raw ligands descriptors',
						required=True)
	parser.add_argument('-m', type=str, dest='method',
						help='For which method are values scaled',
						required=True)
	return vars(parser.parse_args())


def get_max_for_all(frags):
	number = len(next(iter(frags.values())))
	max_desc = []
	max = -1000.0

	for i in range(0, number):
		for frag in frags:
			if float(frags[frag][i]) > max:
				max = float(frags[frag][i])
		max_desc.append(max)
		max = -10000.0
	return max_desc


def get_min_for_all(frags):
	number = len(next(iter(frags.values())))
	min_desc = []
	min = 1000.0

	for i in range(0, number):
		for frag in frags:
			if float(frags[frag][i]) < min:
				min = float(frags[frag][i])
		min_desc.append(min)
		min = 10000.0
	return min_desc


# Scaling and binning for 2. phase - do not throw away constat descriptors
def scalling_binning_phase_2(scaling_max, scaling_min, desc_raw, simple, head_split):
	desc_number = len(next(iter(desc_raw.values())))
	result = {}

	for frag in desc_raw:
		result[frag] = []
		for i in range(0, desc_number):
			# Do not throw away constat descriptors in phase 2!! It would mess up the order of weights from phase_1!!
			if float(scaling_max[i]) == float(scaling_min[i]):
				scaled = 0
				result[frag].append(scaled)
			else:
				x = float(desc_raw[frag][i])
				# formula scaled = ((x - min_desc) / (max_desc - min_desc)) - scaling
				scaled = ((x - float(scaling_min[i])) / (float(scaling_max[i]) - float(scaling_min[i])))
				if simple == True:
					# exception because of nan
					try:
						result[frag].append(int(math.floor(scaled * 100)))  # Pro simple metodu (binning)
					except:
						result[frag].append(-1)
				else:
					result[frag].append(scaled)  # Pro eukleid metodu

	return result, head_split


# Scaling and binning for 1. phase - throws away constatn descriptors
def scalling_binning(scaling_max, scaling_min, desc_raw, simple, head_split):
	desc_number = len(next(iter(desc_raw.values())))
	result = {}

	removed = []

	for frag in desc_raw:
		result[frag] = []
		for i in range(0, desc_number):
			# Scaling and binning (100 bins) at the same time
			if float(scaling_max[i]) == float(scaling_min[i]):
				if i not in removed:
					removed.append(i);
				# V tomhle případě se může danej descriptor zanedbat
			else:
				x = float(desc_raw[frag][i])
				# scaled = ((x - min_desc) / (max_desc - min_desc)) - scaling
				scaled = ((x - float(scaling_min[i])) / (float(scaling_max[i]) - float(scaling_min[i])))
				if simple == True:
					# exception because of nan
					try:
						result[frag].append(int(math.floor(scaled * 100)))  # Pro simple metodu (binning)
					except:
						result[frag].append(-1)
				else:
					result[frag].append(scaled)  # Pro eukleid metodu

	removed.sort(reverse=True)

	for item in removed:
		del head_split[item]

	return result, head_split


# Returns names of molecules of given type and activity - for example active test
def get_type(split, x, type):
	if type not in split['data']:
		return []

	array = []
	for item in split['data'][type]:
		if item['activity'] == x:
			array.append(item['name'])
	return array


# Returns names of molecules of train molecules (active or inactive)
def get_train_type(split, type):
	array = []
	for item in split['data']['train'][type]:
		array.append(item['name'])
	return array


# Returns molecule with given name
def get_molecule(name, molecules):
	for mol in molecules:
		if mol['name'] == name:
			return mol
	return None


# Counts the occurence of fragments in molecules
def get_counts(counts, molecules):
	seen = []
	for mol in molecules:
		for frag in mol['fragments']:
			# Counts each fragment only once
			if frag['smiles'] not in seen:
				seen.append(frag['smiles'])
				if frag['smiles'] not in counts:
					counts[frag['smiles']] = 1
				else:
					counts[frag['smiles']] += 1
		seen = []
	return counts


# Returns fragment names which are over the treshold
def frags_over_threshlod(dec_molecules, lig_molecules, ctx):
	frags_over = []

	counts = get_counts({}, dec_molecules)
	counts_final = get_counts(counts, lig_molecules)

	mol_num = len(dec_molecules) + len(lig_molecules)

	treshold = int(float(ctx['treshold'])) / 100
	for frag_smiles in counts_final:
		if (counts[frag_smiles] / mol_num) >= treshold:
			frags_over.append(frag_smiles)

	return frags_over


# Returns molecules which are used in current split and filters out fragments which are over treshold and duplicate fragments
def get_molecule_list(names, molecules_all, frags_over):
	molecules_select = []
	seen = []
	filtered = []
	for mol in names:
		molecule = get_molecule(mol, molecules_all)
		# Molecule without any fragments throw away
		if molecule is None:
			continue
		# Throws away fragments over treshold and duplicate fragments
		for frag in molecule["fragments"]:
			if frag["smiles"] in frags_over or frag["smiles"] in seen:
				pass
			else:
				seen.append(frag["smiles"])
				filtered.append(frag)
		molecule["fragments"] = filtered
		seen = []
		filtered = []
		# Molecule without any fragments throw away
		if not molecule["fragments"]:
			continue
		molecules_select.append(molecule)

	return molecules_select


# Returns used fragments in set of molecules
def used(output, molecules):
	for mol in molecules:
		for frag in mol["fragments"]:
			if frag["smiles"] not in output:
				output.append(frag["smiles"])
	return output


# Writes molecules into file, each fragment is written as a index in mapping of fragments - used as pointers in c++ program screening.
def reindex_out(molecules, mapping, stream):
	first = True
	for mol in molecules:
		for frag in mol["fragments"]:
			if first == False:
				stream.write(" ")
			stream.write(str(mapping[frag["smiles"]]))
			first = False
		first = True
		stream.write("\n")


# In order to identify which fragment has which name - not used at the moment since we did not need it
def write_legend(dictionary, stream):
	for item in dictionary:
		stream.write(item["name"])
		stream.write("\n")


# Returns molecules which names are in a set of names
def get_used_mols(molecules, names):
	used = []
	for mol in molecules:
		if mol['name'] in names:
			used.append(mol)
	return used


# Marks which clusters can be joined - which clusters are correlated above treshold for all their pairs
def clusters_to_join(clusters, matrix, corr_threshold):
	for i in range(len(clusters)):
		for j in range(i + 1, len(clusters)):
			correlated = True
			for corr_i in clusters[i]:
				if not correlated: break
				for corr_j in clusters[j]:
					if abs(matrix[corr_i][corr_j]) < corr_threshold:
						correlated = False
						break
			if correlated: return [i, j]

	return [-1, -1]


# Correlated descriptors are removed using correlation matrix
def correlated_descriptors_elimination(head_split, frags_descriptors_used, configuration):
	descriptors = []
	fragment_names = []
	desc_names = head_split

	for i in range(0, len(desc_names)):
		descriptors.append([])

	for frag in frags_descriptors_used:
		fragment_names.append(frag)
		for i in range(0, len(frags_descriptors_used[frag])):
			descriptors[i].append(frags_descriptors_used[frag][i])

	# nan is exchanged for median
	for i in range(len(descriptors)):
		median = np.median(np.asarray([x for x in descriptors[i] if not math.isnan(x)]))
		descriptors[i] = [median if math.isnan(x) else x for x in descriptors[i]]

	matrix = [[0 for x in range(len(descriptors))] for x in range(len(descriptors))]
	for i in range(len(descriptors)):
		a = np.asarray(descriptors[i])
		for j in range(i, len(descriptors)):
			c = np.corrcoef(a, np.asarray(descriptors[j]))[0, 1]
			matrix[i][j] = c
			matrix[j][i] = c

	# Unique cluster for each descriptor
	clusters = []
	for i in range(len(descriptors)):
		clusters.append([i])

	corr_treshold = float(configuration["correlation"])

	while True:
		[i, j] = clusters_to_join(clusters, matrix, corr_treshold)
		if i == -1: break
		clusters[i] += clusters[j]
		clusters.pop(j)

	# Keep only first descriptor
	desciptor_to_remove = sorted([j for i in clusters for j in i[1:]])
	for desc in reversed(desciptor_to_remove):
		desc_names.pop(desc)
		descriptors.pop(desc)

	final_descriptors = {}
	for i in range(len(descriptors[0])):
		desc = []
		for feature in descriptors:
			desc += [feature[i]]
		final_descriptors[fragment_names[i]] = desc

	return desc_names, final_descriptors


# Deletes only descriptors, which are not used in weights - for phase 2
def delete_descriptors_without_weights(head, frags_descriptors_used, configuration):
	with open(configuration['weights'], 'r') as stream:
		final_head = stream.readline().strip().split(' ')

	throw_away_index = []
	for index in range(0, len(head)):
		if head[index] not in final_head:
			throw_away_index.append(index)

	throw_away_index.sort(reverse=True)

	final_descriptors = {}

	for frag in frags_descriptors_used:
		final_descriptors[frag] = frags_descriptors_used[frag]

	for frag in final_descriptors:
		for index in throw_away_index:
			del final_descriptors[frag][index]

	return final_head, final_descriptors


def _main():
	configuration = _read_configuration()

	if configuration["method"] == "simple":
		simple = True
	else:
		simple = False

	raw_combined_desc = {}

	with open(configuration['decoys_desc_raw'], 'r') as stream:
		# Skip header line.
		head = stream.readline().split(',')
		del head[0]
		reader = csv.reader(stream, delimiter=',', quotechar='"')
		for row in reader:
			if row[0] not in raw_combined_desc:
				raw_combined_desc[row[0]] = row[1:]

	with open(configuration['ligands_desc_raw'], 'r') as stream:
		# Skip header line.
		stream.readline()
		reader = csv.reader(stream, delimiter=',', quotechar='"')
		for row in reader:
			if row[0] not in raw_combined_desc:
				raw_combined_desc[row[0]] = row[1:]

	with open(configuration['dec_fragments'], 'r') as stream:
		dec_molecules = json.load(stream)
	with open(configuration['lig_fragments'], 'r') as stream:
		lig_molecules = json.load(stream)
	with open(configuration['split'], 'r') as stream:
		split = json.load(stream)

	active_molecules_train = []
	inactive_molecules_train = []
	active_molecules_validation = []
	inactive_molecules_validation = []
	active_molecules_test = []
	inactive_molecules_test = []

	if configuration['phase'] == 1:

		active_names_validation = get_type(split, 1, 'validation')
		inactive_names_validation = get_type(split, 0, 'validation')
		active_names_train = get_train_type(split, 'ligands')  # Names of train molecules in split
		inactive_names_train = get_train_type(split, 'decoys')  # Names of train molecules in split

		dec_molecules_used = []
		dec_molecules_used += get_used_mols(dec_molecules, inactive_names_validation)
		dec_molecules_used += get_used_mols(dec_molecules, inactive_names_train)

		lig_molecules_used = []
		lig_molecules_used += get_used_mols(lig_molecules, active_names_validation)
		lig_molecules_used += get_used_mols(lig_molecules, active_names_train)

		frags_over = frags_over_threshlod(dec_molecules_used, lig_molecules_used, configuration)

		# Lists without empty molecules and without fragments above treshold or duplicit fragments
		active_molecules_train = get_molecule_list(active_names_train, lig_molecules, frags_over)
		inactive_molecules_train = get_molecule_list(inactive_names_train, dec_molecules, frags_over)
		active_molecules_validation = get_molecule_list(active_names_validation, lig_molecules, frags_over)
		inactive_molecules_validation = get_molecule_list(inactive_names_validation, dec_molecules, frags_over)

		used_fragments = used(
			used(used(used([], inactive_molecules_train), active_molecules_train), inactive_molecules_validation),
			active_molecules_validation)

		used_frag_desc = {}
		for name in raw_combined_desc:
			if name in used_fragments:
				used_frag_desc[name] = raw_combined_desc[name]

		# Deals with the infinite value
		a = 0
		for frag in used_frag_desc:
			for i in range(0, len(used_frag_desc[frag])):
				try:
					a = float(used_frag_desc[frag][i])
					if np.isinf(a):
						a = float('nan')
					used_frag_desc[frag][i] = a
				except ValueError:
					used_frag_desc[frag][i] = float('nan')

		# Scale values of descriptors into [0,1] and remove constant descriptors
		scaling_vector_max = get_max_for_all(used_frag_desc)
		scaling_vector_min = get_min_for_all(used_frag_desc)

		scaled_used_frag_desc, head_split = scalling_binning(scaling_vector_max, scaling_vector_min, used_frag_desc,
															 simple, head)
		final_head, final_descriptors = correlated_descriptors_elimination(head_split, scaled_used_frag_desc,
																		   configuration)

		counter = 0
		mapping = {}
		for name in final_descriptors:
			mapping[name] = counter
			counter += 1

		if configuration['tmp'] == "True":
			with open(configuration['final_output'] + "_desc_tmp", 'w') as stream:
				for item in final_head:
					stream.write(item.strip())
					stream.write(" ")

	if configuration['phase'] == 2:

		active_names_train = get_train_type(split, 'ligands')  # Names of train molecules in split
		active_names_validation = get_type(split, 1, 'validation')
		active_names_test = get_type(split, 1, 'test')
		inactive_names_test = get_type(split, 0, 'test')

		dec_molecules_used = []
		dec_molecules_used += get_used_mols(dec_molecules, inactive_names_test)

		lig_molecules_used = []
		lig_molecules_used += get_used_mols(lig_molecules, active_names_validation)
		lig_molecules_used += get_used_mols(lig_molecules, active_names_train)
		lig_molecules_used += get_used_mols(lig_molecules, active_names_test)

		frags_over = frags_over_threshlod(dec_molecules_used, lig_molecules_used, configuration)

		# Lists without empty molecules and without fragments above treshold or duplicit fragments
		active_molecules_train = get_molecule_list(active_names_train, lig_molecules, frags_over)
		active_molecules_validation = get_molecule_list(active_names_validation, lig_molecules, frags_over)
		active_molecules_test = get_molecule_list(active_names_test, lig_molecules, frags_over)
		inactive_molecules_test = get_molecule_list(inactive_names_test, dec_molecules, frags_over)

		used_fragments = used(
			used(used(used([], active_molecules_train), active_molecules_validation), active_molecules_test),
			inactive_molecules_test)

		used_frag_desc = {}
		for name in raw_combined_desc:
			if name in used_fragments:
				used_frag_desc[name] = raw_combined_desc[name]

		# Deals with the infinite value
		a = 0
		for frag in used_frag_desc:
			for i in range(0, len(used_frag_desc[frag])):
				try:
					a = float(used_frag_desc[frag][i])
					if np.isinf(a):
						a = float('nan')
					used_frag_desc[frag][i] = a
				except ValueError:
					used_frag_desc[frag][i] = float('nan')

		# Scale values of descriptors into [0,1] and remove constant descriptors
		scaling_vector_max = get_max_for_all(used_frag_desc)
		scaling_vector_min = get_min_for_all(used_frag_desc)
		scaled_used_frag_desc, head_split = scalling_binning_phase_2(scaling_vector_max, scaling_vector_min,
																	 used_frag_desc, simple, head)

		# Deletes descriptors, which are not used when calculating weights in phase 1
		final_head, final_descriptors = delete_descriptors_without_weights(head_split, scaled_used_frag_desc,
																		   configuration)

		counter = 0
		mapping = {}
		for name in final_descriptors:
			mapping[name] = counter
			counter += 1

	with open(configuration['final_output'], 'w') as stream:
		stream.write(str(len(final_head)))
		stream.write("\n")
		stream.write(str(len(mapping)))
		stream.write("\n")
		stream.write(str(len(active_molecules_train)))
		stream.write("\n")
		stream.write(str(len(active_molecules_validation)))
		stream.write("\n")
		stream.write(str(len(active_molecules_test)))
		stream.write("\n")
		stream.write(str(len(inactive_molecules_train)))
		stream.write("\n")
		stream.write(str(len(inactive_molecules_validation)))
		stream.write("\n")
		stream.write(str(len(inactive_molecules_test)))
		stream.write("\n")
		first = True
		for name in final_descriptors:
			for desc in final_descriptors[name]:
				if first == False:
					stream.write(" ")
				stream.write(str(desc))
				first = False
			first = True
			stream.write("\n")
		reindex_out(active_molecules_train, mapping, stream)
		reindex_out(active_molecules_validation, mapping, stream)
		reindex_out(active_molecules_test, mapping, stream)
		reindex_out(inactive_molecules_train, mapping, stream)
		reindex_out(inactive_molecules_validation, mapping, stream)
		reindex_out(inactive_molecules_test, mapping, stream)

		# Vypíše legendu k datům
		for item in final_head:
			stream.write(item.strip())
			stream.write(" ")

		# for name in complete_desc:
		#	if name in used_fragments:
		#		stream.write(name)
		#		stream.write("\n")

		# write_legend(train_molecules, stream)
		# write_legend(active_molecules_test, stream)
		# write_legend(inactive_molecules_test, stream)
		# write_legend(active_molecules_validation, stream)
		# write_legend(inactive_molecules_validation, stream)


if __name__ == '__main__':
	_main()
