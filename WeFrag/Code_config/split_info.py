import argparse

"""
From .sdf files creates a information about a split in a JSON format.

-ted -- test_decoys.sdf
-trd - train_decoys.sdf
-vad - validation_decoys.sdf
-tel - test_actives.sdf
-trl - train_actives.sdf
-val - validation_actives.sdf
-o - Output file
"""


def _read_configuration():
	parser = argparse.ArgumentParser(
		description='Split info generation')
	parser.add_argument('-ted', type=str, dest='test_d',
						help='Input test .sdf',
						required=False)
	parser.add_argument('-trd', type=str, dest='train_d',
						help='Input train .sdf',
						required=False)
	parser.add_argument('-vad', type=str, dest='validation_d',
						help='Input validation .sdf', required=False)
	parser.add_argument('-tel', type=str, dest='test_l',
						help='Input test .sdf',
						required=False)
	parser.add_argument('-trl', type=str, dest='train_l',
						help='Input train .sdf',
						required=False)
	parser.add_argument('-val', type=str, dest='validation_l',
						help='Input validation .sdf', required=False)
	parser.add_argument('-o', type=str, dest='output',
						help='Output file', required=True)

	return vars(parser.parse_args())


def process_line(line, name_found, file, type, activity, what):
	if name_found == True:
		file.write(what)
		if type == "train":
			file.write("    {\n")
			file.write("     \"name\": " + "\"" + line.strip() + "\"\n")
			file.write("    }")
		else:
			file.write("   {\n")
			file.write("    \"name\": " + "\"" + line.strip() + "\",\n")
			file.write("    \"activity\": " + str(activity) + "\n")
			file.write("   }")
		return False, True, ""
	else:
		if line.strip() == "$$$$":
			return True, False, what
		return False, False, what


# Processes lines which at the end need to end without comma
def process_nocomma(o, f, type, activity):
	name_found = True;
	comma = False
	after_name = False
	what = ""
	for line in f:
		if comma == True:
			if after_name == True:
				what = ",\n"
		else:
			if after_name == True:
				what = "\n"
		name_found, after_name, what = process_line(line, name_found, o, type, activity, what)
		comma = True
	o.write("\n")


# Processes lines which at the end need to end with comma
def process_wcomma(o, f, type, activity):
	name_found = True;
	comma = False
	what = ""
	for line in f:
		if comma == True:
			what = ",\n"
		name_found, after_name, dont_matter = process_line(line, name_found, o, type, activity, what)
		comma = True
	o.write(",\n")


def _main():
	configuration = _read_configuration();

	# Does not fail, if the file does not exist
	with open(configuration["output"], "w+") as o:
		o.write("{\n")
		o.write(" \"data\": {\n")
		o.write("  \"test\": [\n")
		if "test_d" in configuration:
			try:
				with open(configuration["test_d"]) as f:
					process_wcomma(o, f, "test", 0)
			except:
				pass
		if "test_l" in configuration:
			try:
				with open(configuration["test_l"]) as f:
					process_nocomma(o, f, "test", 1)
			except:
				pass
		o.write("  ],\n")
		o.write("  \"validation\": [\n")
		if "validation_d" in configuration:
			try:
				with open(configuration["validation_d"]) as f:
					process_wcomma(o, f, "validation", 0)
			except:
				pass
		if "validation_l" in configuration:
			try:
				with open(configuration["validation_l"]) as f:
					process_nocomma(o, f, "validation", 1)
			except:
				pass
		o.write("  ],\n")
		o.write("  \"train\": {\n")
		o.write("   \"decoys\": [\n")
		if "train_d" in configuration:
			try:
				with open(configuration["train_d"]) as f:
					process_nocomma(o, f, "train", 0)
			except:
				pass
		o.write("   ],\n")
		o.write("   \"ligands\": [\n")
		if "train_l" in configuration:
			try:
				with open(configuration["train_l"]) as f:
					process_nocomma(o, f, "train", 1)
			except:
				pass
		o.write("   ]\n")
		o.write("  }\n")
		o.write(" }\n")
		o.write("}\n")


if __name__ == '__main__':
	_main()
