\contentsline {chapter}{Introduction}{3}{chapter*.2}
\contentsline {chapter}{\numberline {1}Introduction to virtual screening and related theory}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}What is virtual screening and its categories}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Ligand based virtual screening with descriptors}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}Molecular Fragments}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}RDKit and PaDEL descriptors}{9}{section.1.4}
\contentsline {section}{\numberline {1.5}Scoring - ROC and AUC}{9}{section.1.5}
\contentsline {section}{\numberline {1.6}Simmulated Annealing}{10}{section.1.6}
\contentsline {chapter}{\numberline {2}Our approach to similarity searching LBVS}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Fragment-feature molecular representation}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Weights}{14}{section.2.2}
\contentsline {section}{\numberline {2.3}Why use fragments}{15}{section.2.3}
\contentsline {section}{\numberline {2.4}Why use weights and how to find them}{15}{section.2.4}
\contentsline {section}{\numberline {2.5}Similarity Functions}{16}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Euclidean Similarity}{16}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Manhattan Similarity}{17}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Simple Matching Similarity}{17}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Nfrag Similarity}{18}{subsection.2.5.4}
\contentsline {chapter}{\numberline {3}Finding the weights}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Implementation of simulated annealing}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}Descriptor scaling and reducing fragment noise}{20}{section.3.2}
\contentsline {section}{\numberline {3.3}Removing correlated descriptors}{20}{section.3.3}
\contentsline {section}{\numberline {3.4}Training and testing weights}{21}{section.3.4}
\contentsline {section}{\numberline {3.5}The whole method put together}{22}{section.3.5}
\contentsline {chapter}{\numberline {4}WeFrag}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}All options WeFrag provides}{23}{section.4.1}
\contentsline {section}{\numberline {4.2}Optimization}{24}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Caching}{25}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}C++ vs Python}{25}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Better sorting algorithm}{25}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Memory optimization}{26}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Reusing the preprocessed data}{26}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Parallelization}{26}{subsection.4.2.6}
\contentsline {chapter}{\numberline {5}Experiments}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}Data used}{29}{section.5.1}
\contentsline {section}{\numberline {5.2}Focus of experiments}{29}{section.5.2}
\contentsline {section}{\numberline {5.3}Results of experiments}{30}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Fragments, descriptors, similarity functions}{30}{subsection.5.3.1}
\contentsline {subsubsection}{Train Data}{31}{section*.3}
\contentsline {subsubsection}{Test Data}{33}{section*.4}
\contentsline {subsubsection}{Conclusion}{35}{section*.5}
\contentsline {subsection}{\numberline {5.3.2}Correlated descriptors}{35}{subsection.5.3.2}
\contentsline {subsubsection}{Conclusion}{36}{section*.6}
\contentsline {subsection}{\numberline {5.3.3}Fragment noise reduction treshold}{36}{subsection.5.3.3}
\contentsline {subsubsection}{Conclusion}{37}{section*.7}
\contentsline {subsection}{\numberline {5.3.4}Temperature, cooling, distance and number of dimension to change}{37}{subsection.5.3.4}
\contentsline {subsubsection}{Conclusion}{40}{section*.8}
\contentsline {subsection}{\numberline {5.3.5}Effects of larger train data}{40}{subsection.5.3.5}
\contentsline {subsubsection}{Conclusion}{41}{section*.9}
\contentsline {subsection}{\numberline {5.3.6}Comparison to traditional methods}{41}{subsection.5.3.6}
\contentsline {subsubsection}{Conclusion}{43}{section*.10}
\contentsline {chapter}{\numberline {6}Discussion}{45}{chapter.6}
\contentsline {section}{\numberline {6.1}Future work}{45}{section.6.1}
\contentsline {chapter}{Bibliography}{47}{chapter*.11}
\contentsline {chapter}{List of Figures}{49}{chapter*.12}
\contentsline {chapter}{List of Tables}{51}{chapter*.13}
\contentsline {chapter}{List of Abbreviations}{53}{chapter*.14}
\contentsline {chapter}{Attachments}{55}{chapter*.15}
\contentsline {chapter}{\numberline {A}File Formats and Directory System}{57}{appendix.A}
\contentsline {section}{\numberline {A.1}Input data format and file system}{57}{section.A.1}
\contentsline {section}{\numberline {A.2}The output of phase\textunderscore preprocess.py}{57}{section.A.2}
\contentsline {section}{\numberline {A.3}Output format}{57}{section.A.3}
\contentsline {chapter}{\numberline {B}Programmer Documentation}{59}{appendix.B}
\contentsline {section}{\numberline {B.1}Client-Server Communication}{60}{section.B.1}
\contentsline {section}{\numberline {B.2}Creating molecular representation - data preprocessing}{61}{section.B.2}
\contentsline {section}{\numberline {B.3}Creating New Similarity Function}{62}{section.B.3}
\contentsline {section}{\numberline {B.4}Writing Custom Optimization Algorithm}{62}{section.B.4}
\contentsline {chapter}{\numberline {C}User Documentation}{63}{appendix.C}
\contentsline {section}{\numberline {C.1}System Requirements}{63}{section.C.1}
\contentsline {section}{\numberline {C.2}Setup}{63}{section.C.2}
\contentsline {section}{\numberline {C.3}Input - commands and data}{64}{section.C.3}
\contentsline {section}{\numberline {C.4}Examples of Usage}{65}{section.C.4}
