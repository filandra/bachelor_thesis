\chapter{Our approach to similarity searching LBVS}

With underlying concepts such as molecular fragments, descriptors and scoring explored in previous chapter, we can finally put together our approach. In this chapter, we focus on individual pieces of our method: the way of representing a molecule, why we think this approach can be succesful, the concept of weights and similarity functions.

\section{Fragment-feature molecular representation}

\smallskip
The core of every ligand based virtual screening method lies in the molecular representation. The traditional way is to describe a molecule as a whole using set of 1D and 2D descriptors in form of vector or a fingerprint while each descriptor has the same priority in comparison.

\smallskip
Our molecular representation differs significantly. Instead of describing features of a molecule as a whole, we describe features of fragments of given molecule. Same set of descriptors is used for every fragment of each molecule - this allows us to compare any pair of fragments. Duplicit fragments are removed from molecules, since they decrease the performance - each molecule would be full of basic fragments like strings of carbon atoms and unique fragments would be hidden.

\smallskip
Now each molecule is represented by a set of vectors (instead of single vector), where the size of the set depends on number of fragments extracted from molecule (which depends on size of given molecule) and dimension of vectors depends on number of descriptors used. This approach allows us to look at molecule from different perspective - we can for example determine that two molecules, which are overall different, are very similar in half of their fragments, even if those fragments are not identical. Perspective such as this can be crucial, if those fragments are responsible for activity of the molecule. This distinction is not possible with traditional representations.

\smallskip
This representation is called fragment-feature molecular representation and was proposed by Hoksza and Škoda in \cite{HokszaSkoda}.

\smallskip
Creation of representation consist of two steps:
\begin{enumerate}
\item Split molecules into suitable fragments
\item Calculate descriptors for each fragment
\end{enumerate} 

\smallskip
\textit{First step:}

One can split a molecule into fragments in many different ways, so how to decide which way is the most suitable? First we had to consider which technical solution is best for us - we decided to use RDKit to generate linear and circular fragments. Now with our focus narrowed to linear and circular fragments we chose to explore linear fragments of lengths two, three, four and circular fragments of radius one and two. Larger fragments for both types were computationally too demanding. Which type of fragments is most suitable one is determined experimentally in section \ref{Fragments}.

\smallskip
\textit{Second step:}

PaDEL and RDKit were chosen to calculate descriptors. The reason being both PaDEL and RDKit are opensource and PaDEL calculates greater number of descriptors than RDKit. We compare how the number of descriptors influence performance of our method experimentally in section \ref{Fragments}. 

\smallskip
With this representation in hand we can then add weights to individual descriptors and compare two molecules using a similarity function on which we focus in section \ref{Similarity}.

\section{Weights}

\smallskip
In traditional methods, each descriptor used in molecular representation has the same priority during comparison of molecules. In our approach each descriptor is given weight which influences its priority when comparing two fragments \ref{weighted_fragment_feature}. 

\smallskip
Weights essentialy allow us to specify which features are more important when deciding similarity to known active compounds than others (e.g. difference in the number of carbon atoms would affect similarity more than difference in the number of hydrogen atoms). We hypothesize that when the right features are prioritised above others (in a meaningful way) we can improve the precision of similarity search between known actives and candidate molecules.

\begin{figure}[!htbp]
  \caption{Diagram of weighted fragment-feature molecular representation}
  \label{weighted_fragment_feature}
  \centering
    \includegraphics[width=\textwidth]{../img/fragment-feature.png}
\end{figure}

\smallskip
It is important that those weights are not universal for all molecules, but are specific for given known active molecules and a target. Essentialy each pair of known actives and a target has its own set of weights, which helps to identify other active molecules in a candidate set. The reason being that for different known sets of active molecules and different targets the features which are important are different too.


\section{Why use fragments}
\smallskip
Lets compare the use of fragment-feature representation to other methods.

\smallskip
First lets discuss fingerprints (topological torsions, ECFP, MACCS). They are somewhat similar in nature thanks to the focus on fragments - although fingerprints focus mainly on presence or absence of fragments. However, our method can distinguishe how similar fragments are. We think that comparison of active and candidate molecules should focus more on presence or absence of crucial properties of fragments rather than on presence or absence of whole fragments. 

\smallskip
Lets imagine this situation: we have three molecules - one known active and two candidates - and we take two pairs active+candidate. We also calculate that having an aromatic ring is crucial property for active molecule so we give this descriptor high weight. First pair does not share any fragments, but some fragments of candidate molecule have aromatic rings. Second pair share many fragments but not a single fragment of candidate molecule has aromatic ring. If we used fingerprints, we would declare the second pair much more similar than the first one. With our approach the first pair can be more similar than the second one (when the right similarity function is used). If in this situation having aromatic ring really was the reason for activity of a compound, our method would prove to be more useful.

\smallskip
Second, lets take a look at traditional descriptor methods (single vector of descriptors for molecule). We think that the ability to look at molecule in fragments is more useful than the global way of looking at a molecule. For example if we used one descriptor per molecule and determined that two molecules have similarity of 0.5 (on a scale from 0 to 1) using Euclidean distance, we would not know, if the two molecules are similar in mediocre way overall or the two molecules have parts which are completly identical and other parts which are completly different. With our method we can tell the difference (with the right similarity function) and even tell, if the similar side is important thanks to weighted descriptors. If only a part of molecule is important for a molecule to be active, our method would be more useful.

\smallskip
Another advantage of our method is the ability to change what similarity means with great range of similarity functions which can work with this representation.

However the disadvantage of our method is that it is more computationally demanding than the other methods.

\section{Why use weights and how to find them}
%Chapter 1 - str 9 a dále, dobrý info
\smallskip
Lets suppose we would like to compare humans for various reasons. Each human is described by a set of descriptors - i.e. height, inteligence, strength, facial features, genetic code. Now we would like to know the chance that 2 random people are related. One option is to take in account all features and estimate, that people which are the most similar over all should be related. This is obviously not so great idea. We can clearly see, that we have a descriptor ideal for this comparison - the genetic code. Other features can throw our calculation off.

\smallskip
Now lets compare people to a basketball player and calculate the chance, that this person is also a basketball player. In this case it is not so clear, which descriptors are important, but facial features will probably be the least important descriptor. 

\smallskip
For different comparisons different descriptors are useful which also applies to molecules. The difference is that when comparing molecules, it is almost impossible to determine, which descriptors are useful and which are not in advance. Althought almost certainly not every feature should be equally important. That is why each descriptor has a weight which describes how much important it is (0\% as not relevant to 100\% maximum importance) .

\smallskip
So now we know that we want to assign weights to descriptors. The question is how to find the best weights. We can imagine virtual screening being multidimensional continuous function which gives us a value from 0 to 1 (AUC value) for each  set of coordinates (different weights). We would like to find a set of coordinates which gives us the global maxima of AUC in this multidimensional space. If we had only 3 descriptors, we can imagine that we search for highest peak in a cube which contains some sort of a landscape. If we had infinite computational power we could just calculate AUC for each set of weights and find the maximum value. However since we do not have such luxury, we need to find some other method. This is common optimisation problem and the variety of solutions is great - hillclimbing, beamsearch, simulated annealing, tabu search, harmony search, genetic algorithms, parallel tempering etc. We chose to use simulated annealing. The implementation of simulated annealing is described in section \ref{Annealing}.

\section{Similarity Functions}
\label{Similarity}

\smallskip
With our representation created we now face the problem of choosing the right similarity function for comparing molecules. Since we represent molecules as sets of fragments and we represent fragments as vectors, essentially we want to determine the similarity of two sets of vectors. Different similarity functions allow us to take different perspective and influence outcome of the method greatly - for example we can focus on average similarity of fragments (global perspectvie) or focus on the most similar fragments (narrow perspective) etc.

\smallskip
No matter which similarity function was used, the final similarity assigned to a candidate molecule is always calculated as $\max (s(x,a) : a \in A)$m where $s$ is the similarity function, $x$ is candidate molecule and $A$ is set of known actives. Formulas in this section use following symbols: $M_1$ and $M_2$ are compared molecules, $f_1$ and $f_2$ are fragments from those molecules, $Descriptors$ are indexes which identifies parts of vector of descriptors, $weights$ is an array of weights, one weight for each descriptor. The $f_1(d)$ and $f_2(d)$ are values of $d$ descriptor in their fragments, ${\mid}M_1{\mid}$ and ${\mid}M_2{\mid}$ is the number of fragments of molecule $M_1$ or $M_2$. We implemented 6 similarity functions:

\subsection{Euclidean Similarity}

\smallskip
Euclidean similarity is a popular way of computing similarity between two sets of vectors. We simply measure the euclidean distance between all fragments from the two compared molecules and average the sum to get a distance of molecules and then substract that from 1 to get similarity: 

\begin{equation}
1 - {\sum\limits_{f_1 \in M_1, f_2 \in M_2}{\sqrt{\sum\limits_{d \in Descriptors}(f_1(d)-f_2(d)){^2}*weights(d)}}\over{{\mid}M_1{\mid}\times{\mid}M_2{\mid}\times\sqrt{{\mid}Descriptors{\mid}}}}
\end{equation}

For every pair of fragments $f_1$ and $f_2$ from molecule $M_1$ and $M_2$ we compute the euclidean distance of the descriptors vectors. We then average the sum by ${\mid}M_1{\mid}\times{\mid}M_2{\mid}$ (number of pairs of fragments from both molecules). The division by square root of the number of descriptors will scale our value into [0,1] - since each descriptor can have maximal value of 1 (thanks to scaling of descriptor values which we did in the pre-processing phase) ${\mid}\sqrt{Descriptors}{\mid}$  is the maximal value which euclidean distance of two molecules can have. For purpose of our work we also added weights to the equations.

\smallskip
Time complexity of this method is $O{\mid}M_1{\mid}\times{\mid}M_2{\mid}\times{\mid}Descriptors{\mid}$  - which is the number of $(f_1(d)-f_2(d))^2$ calculated.

\subsection{Manhattan Similarity}

\smallskip
Manhattan similarity uses taxicab geometry in which distance of two points is defined as the sum of diferences of each dimension. The whole formula is:

\begin{equation}
1 - {\sum\limits_{f_1 \in M_1, f_2 \in M_2}{\sqrt{\sum\limits_{d \in Descriptors}{\mid}f_1(d)-f_2(d){\mid}*weights(d)}}\over{{\mid}M_1{\mid}\times{\mid}M_2{\mid}\times{\mid}Descriptors{\mid}}}
\end{equation}

Thus the only difference between Euclidean and Manhattan similarity is the change of $(f_1(d)-f_2(d))^2$ to ${\mid}f_1(d)-f_2(d){\mid}$ when calculating fragment distance and change of division by $\sqrt{{\mid}Descriptors{\mid}}$ to ${\mid}Descriptors{\mid}$. Time complexity stays the same as $O{\mid}M_1{\mid}\times{\mid}M_2{\mid}\times{\mid}Descriptors{\mid}$ .

\subsection{Simple Matching Similarity}

This strategy utilises binned values of fragment vectors. Binning is done in the pre-processing phase. For each descriptor we find the minimal and maximal value in all active and candidate compounds. Minimal value goes to the first bin, maximal to the last bin and values in between are assigned accordingly depending on the number of bins. Similarity of two fragments is then computed as number of shared bins divided by number of all bins. Similarity of two molecules is then determined as mean of fragment pairs similarities. Again weights were added for the purpose of this work: 

\begin{equation}
{\sum\limits_{f_1 \in M_1, f_2 \in M_2}\sum\limits_{d \in Descriptors} {\big(}Indicator(f_1(d) , f_2(d))*weights(d){\big)}}\over{{\mid}M_1{\mid}\times{\mid}M_2{\mid}\times{\mid}Descriptors{\mid}}
\end{equation}

\begin{equation}
Indicator(f_1(d) , f_2(d)) = \begin{cases} 1 & \text{if } f_1(d)=f_2(d) \\
                      0    & \text{if } f_1(d) \neq f_2(d)  %
        \end{cases} 
\end{equation} 

\smallskip
The disadvantage of this method is that we do not measure how different the values are, we just determine if they are or are not similar enough (the threshold is given by the number of bins). Time complexity is $O({\mid}M_1{\mid}\times{\mid}M_2{\mid}\times{\mid}Descriptors{\mid})$. 

\subsection{Nfrag Similarity}
\label{Nfrag}

\smallskip
The disadvantage of Euclidean, Manhattan and Simple similarity is that two identical compounds do not have the similarity of 1 (unless they have only 1 fragment), so two identical molecules can be less similar than two different molecules. This can lead to major errors when used to compare candidate molecules and known active ones.

\smallskip
Nfrag similarity attempts to solve the problem stated above. It can be used with any distance function. Instead of calculating distance of two molecules as average of all fragment pairs distances we choose $N$ of the most similar pairs. In fact the number N is different for every pair of molecules:

\begin{equation}
N=\floor*{{\max({\mid}M_1{\mid}, {\mid}M_2{\mid})}*{({P}\over{100})}}
\end{equation}

Formula for similarity:

\begin{equation}
1 - {{\sum\limits_{i=1}^{N} SortedFragDist(i)}\over{N\times\sqrt{{\mid}Descriptors{\mid}}}}
\end{equation} 

\begin{equation}
SortedFragDist=Sort{\bigg(}{\bigcup\limits_{f_1 \in M_1, f_2 \in M_2} {Distance(f1, f2)}}{\bigg)}
\end{equation} 

\smallskip
$Distance$ is function which computes distance of two fragments using all descriptors - we use simple, Euclidean and Manhattan distance.

\smallskip
$Sort$ is any sorting function which sorts a set of numbers in descending order.

\smallskip
This way two identical molecules have the similarity value of 1, because for every fragment of one molecule exist the same fragment in the other. Also because we throw away duplicate fragments (in the pre-processing phase) the only other way of obtaining the similarity value of 1 would be by comparing two molecules which have only 1 type of fragment, however, in different numbers. For example the fragment is pair of carbon atoms and $M_1$ consist of two of those fragments and $M_2$ consist of three of those fragments - these molecules are not exactly the same but our method would give them similarity of 1. Which certainly is a disadvantage of this method, however, this situation does not occur very often. 

\smallskip
Just note that for any other $P$ than 100 this method will again have the same disadvantage as simple, Euclidean and Manhattan similarity.

\smallskip
Time complexity of this method is $O({\mid}M_1{\mid}\times{\mid}M_2{\mid}\times{\mid}Descriptor{\mid}+{\mid}M_1{\mid}\times{\mid}M_2{\mid}\times\log({\mid}M_1{\mid}\times{\mid}M_2{\mid}))$ 
where first half of the sum represents the time complexity of fragment pairs comparison while the second half represents sorting of the distance to obtain $SortedFragDist$.